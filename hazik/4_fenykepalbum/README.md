# Felhőalapú elosztott rendszerek laboratórium

## 4. és 5. házi feladat: fényképalbum létrehozása

## Bálint Gergő [O78UXU], Szommer Zsombor [MM5NOT]

A feladatot több különálló részre bontottuk, ezek a következők:

 - Adatbázis: Postgres DB külön docker image-ben
 - Backend: Rust server külön docker image-ben
   - forrás file-ok és Dockerfile a photo-album-service almappában
 - Frontend: Flutter web app külön docker image-ben
   - forrás file-ok és Dockerfile a photo_album_client almappában

A konténerizálást Docker compose segítségével végeztük. PaaS-nek a [Railway-t](https://railway.app/) használjuk.
Itt, GitLab-on hoztunk létre CI/CD pipeline-t az automatikus deployment-hez.
Ennek forrása a [repo gyökerében](https://gitlab.com/Melidon/felhoalapu-elosztott-rendszerek-laboratorium/-/tree/master?ref_type=heads) található.

### A Backend és a frontend közti kommunikáció RestAPI segítségével zajlik, ennek leírása a következő:

- **/api/auth/sign-up**
  - POST
  - body:
    ```json
    {
        "username": "melidon",
        "password": "almafa"
    }
    ``` 
  - response 201 Created:
    ```json
    {
        "access_token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjE3MTM3OTY2MDUsInVzZXJfaWQiOiI3MGY0NzAxMy0zMmViLTQ5NTItYjMwZS1kMTMxYjcwMmZjYmYifQ.smjcco0Aexkdg8C0h0NQmfTLo1sgzeh-TYJpBxqQ5F4",
        "token_type": "Bearer"
    }
- **/api/auth/sign-up**
  - GET
  - body:
    ```json
    {
        "username": "melidon",
        "password": "almafa"
    }
    ``` 
  - response 200 OK:
    ```json
    {
        "access_token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjE3MTM3OTY2MDUsInVzZXJfaWQiOiI3MGY0NzAxMy0zMmViLTQ5NTItYjMwZS1kMTMxYjcwMmZjYmYifQ.smjcco0Aexkdg8C0h0NQmfTLo1sgzeh-TYJpBxqQ5F4",
        "token_type": "Bearer"
    }
- **/api/photos**
  - POST
  - Headers:
    - Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjE3MTM3OTY2MDUsInVzZXJfaWQiOiI3MGY0NzAxMy0zMmViLTQ5NTItYjMwZS1kMTMxYjcwMmZjYmYifQ.smjcco0Aexkdg8C0h0NQmfTLo1sgzeh-TYJpBxqQ5F4
  - Multipart form data
    - the name of your photo: your_photo.jpg
    - Only jpeg and png files are allowed
    - Ony files under 2MB are allowed
    - Only the first photo is stored, the rest is ignored
  - response 201 Created:
    ```json
    {
        "id": "c71be16e-84fa-4ed1-b54b-fdd464b6766a"
    }
- **/api/photos/c71be16e-84fa-4ed1-b54b-fdd464b6766a**
  - GET
  - Headers:
    - Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjE3MTM3OTY2MDUsInVzZXJfaWQiOiI3MGY0NzAxMy0zMmViLTQ5NTItYjMwZS1kMTMxYjcwMmZjYmYifQ.smjcco0Aexkdg8C0h0NQmfTLo1sgzeh-TYJpBxqQ5F4
  - Response 200 OK: the photo
- **/api/photos**
  - GET
  - Headers:
    - Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjE3MTM3OTY2MDUsInVzZXJfaWQiOiI3MGY0NzAxMy0zMmViLTQ5NTItYjMwZS1kMTMxYjcwMmZjYmYifQ.smjcco0Aexkdg8C0h0NQmfTLo1sgzeh-TYJpBxqQ5F4
  - response 200 OK:
    ```json
    {
        "photos": [
            {
                "date": "2024-04-21T21:50:12.948031",
                "id": "c71be16e-84fa-4ed1-b54b-fdd464b6766a",
                "name": "Tuxos"
            }
        ]
    }
    ```
- **/api/photos/c71be16e-84fa-4ed1-b54b-fdd464b6766a**
  - DELETE
  - Headers:
    - Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjE3MTM3OTY2MDUsInVzZXJfaWQiOiI3MGY0NzAxMy0zMmViLTQ5NTItYjMwZS1kMTMxYjcwMmZjYmYifQ.smjcco0Aexkdg8C0h0NQmfTLo1sgzeh-TYJpBxqQ5F4
  - Response: 204 No Content

  
### A Flutter frontend segítségével telefonon is reszponzívan jelenik meg az alkalmazás. Funkciói:
 - Sign-in, logout, Sign-Up
 - Photo feltöltése (jpeg, png, max 2MB )
 - Photo-k listázása
 - Photo törlése
 - Photo lista rendezése név szerint növekvő, illetve dátum szerint csökkenő sorrendbe