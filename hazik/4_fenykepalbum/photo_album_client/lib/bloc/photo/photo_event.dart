part of 'photo_bloc.dart';

/// Events

@immutable
abstract class PhotoEvent {
  const PhotoEvent();
}

class LoadPhotoEvent extends PhotoEvent {
  static final LoadPhotoEvent _instance = LoadPhotoEvent._();

  factory LoadPhotoEvent() => _instance;

  LoadPhotoEvent._();
}