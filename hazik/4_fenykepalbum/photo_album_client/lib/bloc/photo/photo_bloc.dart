import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

import '../../models/photo_body.dart';


part 'photo_event.dart';

part 'photo_state.dart';

/// Questionnaire BLoC is an element of BLoC pattern in MVVM design pattern
/// Manages the QuestionnairePage events and states async

class PhotoBloc extends Bloc<PhotoEvent, PhotoState> {
  final _interactor;

  PhotoBloc(this._interactor) : super(Loading());

  /// This method emits states from incoming events
  @override
  Stream<PhotoState> mapEventToState(PhotoEvent event) async* {
    if (event is LoadPhotoEvent) {
      yield* _mapLoadPhotoToState();
    }
  }

  Stream<PhotoState> _mapLoadPhotoToState() async* {
    try {
      print("getting Photos");
      final photos = await _interactor.getPhotoList();
      print("Successful Http request!");
      yield ContentReady(photos: photos);
    } on Exception catch (e) {
      print("http fail: ${e.toString()}");
      yield Error(photos: const []);
    }
  }
}