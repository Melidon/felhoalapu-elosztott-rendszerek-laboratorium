part of 'photo_bloc.dart';

/// States

@immutable
abstract class PhotoState {
  const PhotoState();
}

class Loading extends PhotoState {
  static final Loading _instance = Loading._();

  factory Loading() => _instance;

  Loading._();
}

class Content extends PhotoState {
  final List<PhotoBody> photos;

  Content({required this.photos});
}

class ContentReady extends Content with EquatableMixin {
  ContentReady({required List<PhotoBody> photos}) : super(photos: photos);

  @override
  List<Object> get props => photos;
}

class Error extends Content with EquatableMixin {
  Error({required List<PhotoBody> photos}) : super(photos: photos);

  @override
  List<Object?> get props => photos;
}