part of 'auth_bloc.dart';


/// States

@immutable
abstract class AuthState {
  const AuthState();
}

class Waiting extends AuthState {
  static final Waiting _instance = Waiting._();

  factory Waiting() => _instance;

  Waiting._();
}

class Loading extends AuthState {
  static final Loading _instance = Loading._();

  factory Loading() => _instance;

  Loading._();
}

class Content extends AuthState {
  final AuthBody token;

  Content({required this.token});
}

class ContentReady extends Content with EquatableMixin {
  ContentReady({required AuthBody token}) : super(token: token);

  @override
  List<Object> get props => [token];
}

class Error extends Content with EquatableMixin {
  Error({required AuthBody token}) : super(token: token);

  @override
  List<Object?> get props => [token];
}