part of 'auth_bloc.dart';

/// Events

@immutable
abstract class AuthEvent {
  const AuthEvent();
}

class RequestAuthEvent extends AuthEvent {
  static final RequestAuthEvent _instance = RequestAuthEvent._();

  factory RequestAuthEvent() => _instance;

  RequestAuthEvent._();
}

class LoadAuthEvent extends AuthEvent {
  static final LoadAuthEvent _instance = LoadAuthEvent._();

  factory LoadAuthEvent() => _instance;

  LoadAuthEvent._();
}

class WrongCredentialEvent extends AuthEvent {
  static final WrongCredentialEvent _instance = WrongCredentialEvent._();

  factory WrongCredentialEvent() => _instance;

  WrongCredentialEvent._();
}