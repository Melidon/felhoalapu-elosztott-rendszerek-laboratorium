import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:equatable/equatable.dart';
import 'package:photo_album_client/bloc/interactor.dart';
import 'package:photo_album_client/models/auth_body.dart';

part 'auth_event.dart';
part 'auth_state.dart';

class AuthBloc extends Bloc<AuthEvent, AuthState> {
  final _interactor;

  AuthBloc(this._interactor) : super(Waiting());

  /// This method emits states from incoming events
  @override
  Stream<AuthState> mapEventToState(AuthEvent event) async* {
    if (event is RequestAuthEvent) {
      yield* _mapWaitTokenToState();
    }

    if (event is LoadAuthEvent) {
      yield* _mapLoadTokenToState();
    }

    if (event is WrongCredentialEvent) {
      yield* _mapContentTokenToState();
    }
  }

  Stream<AuthState> _mapWaitTokenToState() async* {
    try {
      yield Loading();
    } on Exception catch (e) {
      print("fail: ${e.toString()}");
    }
  }

  Stream<AuthState> _mapContentTokenToState() async* {
    try {
      yield Waiting();
    } on Exception catch (e) {
      print("fail: ${e.toString()}");
    }
  }

  Stream<AuthState> _mapLoadTokenToState() async* {
    try {
      final token = await _interactor.signInUp();
      if(token == null) {
        yield Error(token: const AuthBody(accessToken: '', tokenType: 'failed', ));
      }
      yield ContentReady(token: token!);
    } on Exception catch (e) {
      print("http fail: ${e.toString()}");
      yield Error(token: const AuthBody(accessToken: '', tokenType: 'failed', ));
    }
  }
}
