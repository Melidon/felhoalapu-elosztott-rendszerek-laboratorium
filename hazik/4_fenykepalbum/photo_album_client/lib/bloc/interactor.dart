import 'package:dio/dio.dart';
import 'package:photo_album_client/models/auth_body.dart';
import 'package:photo_album_client/models/auth_payload.dart';
import 'package:photo_album_client/models/photo_body.dart';
import 'package:photo_album_client/services/dio/dio_photo_album_service.dart';
import 'package:retrofit/dio.dart';

class Interactor {
  final  PhotoAlbumService _service;
  bool signInState = true;
  String username = '';
  String password = '';
  String token = '';

  Interactor(this._service);

  Future<bool> signIn(String username, String password) async {
    var response = await _service.signIn(AuthPayload(username: username, password: password));
    return response.data != null;
  }

  Future<bool> signUp(String username, String password) async {
    var response = await _service.signUp(AuthPayload(username: username, password: password));
    return response.data != null;
  }

  Future<AuthBody?> signInUp() async {
    HttpResponse<AuthBody?> response;
    if(signInState) {
      response = await _service.signIn(AuthPayload(username: username, password: password));
    } else {
      response = await _service.signUp(AuthPayload(username: username, password: password));
    }
    if(response.data != null) {
      return response.data;
    }
    return null;
  }

  Future<List<PhotoBody>> getPhotoList() async {
    HttpResponse<List<PhotoBody>> response = await _service.list();
    return response.data;
  }

  Future<PhotoBody> postPhoto(MultipartFile image) async {
    HttpResponse<PhotoBody> response = await _service.upload({image.filename!:image});
    return response.data;
  }

  Future<HttpResponse> getPhoto(String id) async {
    HttpResponse response = await _service.get(id);
    return response.data;
  }

  Future<void> deletePhoto(String id) async {
    await _service.delete(id);
  }
}