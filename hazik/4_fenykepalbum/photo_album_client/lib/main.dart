import 'package:flutter/material.dart';
import 'package:photo_album_client/screens/auth_screen.dart';
import 'package:photo_album_client/screens/home_screen.dart';
import 'package:photo_album_client/services/di/di_utils.dart';

import 'bloc/interactor.dart';

final GlobalKey<NavigatorState> navigatorKey = GlobalKey<NavigatorState>();

void main() {
  /// dependency injection
  initDependencies();
  WidgetsFlutterBinding.ensureInitialized();

  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Photo Album',
      navigatorKey: navigatorKey,
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepOrangeAccent),
        useMaterial3: true,
      ),
      routes: {
        '/home': (context) => injector<Interactor>().token == '' ? const AuthScreen() : const HomeScreen(),
        '/auth': (context) => const AuthScreen(),
      },
      initialRoute: '/auth',
    );
  }
}
