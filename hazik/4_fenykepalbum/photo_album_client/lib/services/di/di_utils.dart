import 'package:get_it/get_it.dart';
import 'package:photo_album_client/services/dio/dio_photo_album_service.dart';
import 'package:photo_album_client/services/dio/photo_album_api.dart';

import '../../bloc/auth/auth_bloc.dart';
import '../../bloc/interactor.dart';
import '../../bloc/photo/photo_bloc.dart';


final injector = GetIt.instance;

void initDependencies() {
  injector.registerSingleton<PhotoAlbumApi>(PhotoAlbumService());
  injector.registerFactory(() => PhotoAlbumService());


  injector.registerSingletonAsync(() async {
    return Interactor(
      injector<PhotoAlbumService>(),
    );
  });

  injector.registerFactory(
        () => AuthBloc(
      injector<Interactor>(),
    ),
  );

  injector.registerFactory(
        () => PhotoBloc(
      injector<Interactor>(),
    ),
  );

}