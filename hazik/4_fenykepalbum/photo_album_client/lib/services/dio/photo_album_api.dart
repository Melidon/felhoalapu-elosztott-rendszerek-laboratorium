import 'dart:async';
import 'package:dio/dio.dart';
import 'package:retrofit/retrofit.dart';
import '../../models/auth_body.dart';
import '../../models/auth_payload.dart';
import '../../models/photo_body.dart';

/// Abstract class for an Api calls collection

abstract class PhotoAlbumApi {

  Future<HttpResponse<AuthBody?>> signUp(@Body() AuthPayload authPayload);

  Future<HttpResponse<AuthBody?>> signIn(@Body() AuthPayload authPayload);

  Future<HttpResponse<PhotoBody?>> upload(Map<String, MultipartFile> photos);

  Future<HttpResponse<List<PhotoBody>>> list();

  Future<HttpResponse> get(String id);

  Future<HttpResponse<void>> delete(String id);

}