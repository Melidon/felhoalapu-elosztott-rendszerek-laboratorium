import 'dart:async';
import 'package:dio/dio.dart';
import 'package:photo_album_client/models/auth_body.dart';

import '../../bloc/interactor.dart';
import '../di/di_utils.dart';

/// RequestInterceptor is an Interceptor in connection with Dio http Client
class RequestInterceptor extends Interceptor {
  late AuthBody? authBody;


  @override
  FutureOr<dynamic> onRequest(RequestOptions options, handler) async {
    if(options.path.contains('/photos') && authBody != null){
      options.headers.addAll({
        'Authorization': "${authBody!.tokenType} ${authBody!.accessToken}",
        'Accept-charset': 'UTF-8'
      });
    }
    return handler.next(options);
  }

  @override
  FutureOr<dynamic> onResponse(Response options, handler) async {
    print('OnResponse');
    print(options.requestOptions.path);
    if(options.requestOptions.path.contains('/auth/sign')) {
      authBody = AuthBody.fromJson(options.data);
      print("AuthBody");
      final Interactor _interactor = injector<Interactor>();
      _interactor.token = authBody?.accessToken ?? '';
    }
    return handler.next(options);
  }

  @override
  void onError(DioException err, ErrorInterceptorHandler handler) {
    if (err.response?.statusCode == 500) {
      print(err.response?.statusMessage);
      print(err.response?.data.toString());
    }
    print(err.response?.statusMessage);
    print(err.response?.data.toString());

    return handler.next(err);
  }
}