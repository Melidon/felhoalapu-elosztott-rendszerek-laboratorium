import 'dart:async';
import 'package:dio/dio.dart';
import 'package:photo_album_client/services/dio/request_interceptor.dart';
import 'package:pretty_dio_logger/pretty_dio_logger.dart';
import 'package:retrofit/retrofit.dart';
import '../../models/auth_body.dart';
import '../../models/auth_payload.dart';
import '../../models/photo_body.dart';
import 'photo_album_api.dart';

part 'dio_photo_album_service.g.dart';

@RestApi(baseUrl: "https://rust-production.up.railway.app/api")
abstract class PhotoAlbumService implements PhotoAlbumApi {
  factory PhotoAlbumService() {
    final _dio = Dio();
    _dio.interceptors.add(LogInterceptor());
    _dio.interceptors.add(RequestInterceptor());
    _dio.interceptors.add(PrettyDioLogger(
      requestHeader: true,
      requestBody: true,
      responseBody: false,
      responseHeader: true,
      compact: false,
    ));

    return _PhotoAlbumService(_dio);
  }

  @override
  @POST("/auth/sign-up")
  Future<HttpResponse<AuthBody?>> signUp(@Body() AuthPayload authPayload);

  @override
  @POST("/auth/sign-in")
  Future<HttpResponse<AuthBody?>> signIn(@Body() AuthPayload authPayload);

  @override
  @POST("/photos")
  @MultiPart()
  Future<HttpResponse<PhotoBody>> upload(
    @Part(contentType: "image/jpeg") Map<String, MultipartFile> photos,
  );

  @override
  @GET("/photos")
  Future<HttpResponse<List<PhotoBody>>> list();

  @override
  @GET("/photos/{id}")
  Future<HttpResponse> get(@Path('id') String id);

  @override
  @DELETE("/photos/{id}")
  Future<HttpResponse<void>> delete(@Path('id') String id);
}
