import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:photo_album_client/main.dart';
import 'package:photo_album_client/models/auth_body.dart';
import 'package:photo_album_client/models/auth_payload.dart';
import 'package:photo_album_client/screens/home_screen.dart';
import 'package:photo_album_client/services/dio/dio_photo_album_service.dart';

import '../bloc/auth/auth_bloc.dart';
import '../bloc/interactor.dart';
import '../services/di/di_utils.dart';

class AuthScreen extends StatefulWidget {
  const AuthScreen({super.key});

  @override
  State<AuthScreen> createState() => _AuthScreenState();
}

class _AuthScreenState extends State<AuthScreen> {
  late final _formKey = GlobalKey<FormBuilderState>();
  final _interactor = injector<Interactor>();
  int stateValue = 0;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => injector<AuthBloc>(),
      child: BlocListener<AuthBloc, AuthState>(
        listener: (context, state) {
          if (state is Error) {
            ScaffoldMessenger.of(context).showSnackBar(
              const SnackBar(
                content: Text("Wrong username or password!"),
                elevation: 20,
              ),
            );
            BlocProvider.of<AuthBloc>(context).add(WrongCredentialEvent());
          }
        },
        child: BlocBuilder<AuthBloc, AuthState>(
          builder: (context, state) {
            if (state is Waiting) {
              return Scaffold(
                backgroundColor: Theme.of(context).colorScheme.primaryContainer,
                appBar: AppBar(
                  backgroundColor: Theme.of(context).colorScheme.primary,
                  title: const Text(
                    'Authentication',
                    style: TextStyle(color: Colors.white),
                  ),
                ),
                body: SafeArea(
                  child: Center(
                    child: SingleChildScrollView(
                      child: SizedBox(
                        width: 400,
                        height: 550,
                        child: Card(
                          elevation: 10,
                          child: FormBuilder(
                            key: _formKey,
                            autovalidateMode: AutovalidateMode.always,
                            onChanged: () {
                              setState(() {
                                _formKey.currentState?.save();
                              });
                            },
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              mainAxisSize: MainAxisSize.min,
                              children: [
                                Padding(
                                  padding: const EdgeInsets.symmetric(horizontal: 50, vertical: 20),
                                  child: FormBuilderDropdown(
                                    onChanged: (value) {
                                      stateValue = value ?? 0;
                                    },
                                    name: "radioGroup",
                                    items: const [
                                      DropdownMenuItem(
                                        value: 0,
                                        child: Text("Sign in"),
                                      ),
                                      DropdownMenuItem(value: 1, child: Text("Sign up")),
                                    ],
                                    initialValue: stateValue,
                                  ),
                                ),
                                const SizedBox(
                                  height: 10,
                                ),
                                const Text("Username", style: TextStyle(fontSize: 20, color: Colors.black87)),
                                SizedBox(
                                    width: 180,
                                    child: FormBuilderTextField(
                                      name: 'username',
                                      autofocus: true,
                                      validator: FormBuilderValidators.required(errorText: 'Required field!'),
                                    )),
                                const SizedBox(
                                  height: 20,
                                ),
                                const Text(
                                  "Password",
                                  style: TextStyle(fontSize: 20, color: Colors.black87),
                                ),
                                SizedBox(
                                  width: 180,
                                  child: FormBuilderTextField(
                                      name: 'password',
                                      obscureText: true,
                                      autofocus: false,
                                      validator: FormBuilderValidators.required(errorText: 'Required field!')),
                                ),
                                const SizedBox(
                                  height: 20,
                                ),
                                Visibility(
                                  visible: stateValue == 1,
                                  child: const Text("Confirm password",
                                      style: TextStyle(fontSize: 20, color: Colors.black87)),
                                ),
                                Visibility(
                                  visible: stateValue == 1,
                                  child: SizedBox(
                                    width: 180,
                                    child: FormBuilderTextField(
                                        name: 'confirmPassword',
                                        obscureText: true,
                                        autofocus: false,
                                        validator: stateValue == 1
                                            ? FormBuilderValidators.required(errorText: 'Required field!')
                                            : null),
                                  ),
                                ),
                                const SizedBox(
                                  height: 20,
                                ),
                                MaterialButton(
                                  height: 45,
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(12.0),
                                  ),
                                  color: Theme.of(context).colorScheme.primary,
                                  child: const Text(
                                    "Submit",
                                    style: TextStyle(fontSize: 18, color: Colors.white),
                                  ),
                                  onPressed: () {
                                    int? mode = _formKey.currentState?.value['radioGroup'];
                                    if (mode != null) {
                                      if (mode == 0) {
                                        /// login
                                        String? username = _formKey.currentState?.value['username'];
                                        String? password = _formKey.currentState?.value['password'];
                                        if (username != null && password != null) {
                                          /// Interactor innen
                                          _interactor.signInState = true;
                                          _interactor.username = username;
                                          _interactor.password = password;
                                          BlocProvider.of<AuthBloc>(context).add(RequestAuthEvent());
                                        }
                                      } else {
                                        /// sign up
                                        String? username = _formKey.currentState?.value['username'];
                                        String? password = _formKey.currentState?.value['password'];
                                        String? confirmPassword = _formKey.currentState?.value['confirmPassword'];
                                        if (username != null && password != null && confirmPassword != null) {
                                          /// Interactor innen
                                          _interactor.signInState = false;
                                          _interactor.username = username;
                                          _interactor.password = password;
                                          BlocProvider.of<AuthBloc>(context).add(RequestAuthEvent());
                                        }
                                      }
                                    }
                                  },
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              );
            }
            if (state is Loading) {
              BlocProvider.of<AuthBloc>(context).add(LoadAuthEvent());
              return Scaffold(
                backgroundColor: Theme.of(context).colorScheme.primaryContainer,
                appBar: AppBar(
                  backgroundColor: Theme.of(context).colorScheme.primary,
                  title: const Text(
                    'Authentication',
                    style: TextStyle(color: Colors.white),
                  ),
                ),
                body: Center(
                  child: CircularProgressIndicator(
                    color: Theme.of(context).colorScheme.primary,
                  ),
                ),
              );
            }
            if (state is Content) {
              return const HomeScreen();
            } else {
              return Scaffold(
                backgroundColor: Theme.of(context).colorScheme.primaryContainer,
                appBar: AppBar(
                  backgroundColor: Theme.of(context).colorScheme.primary,
                  title: const Text(
                    'Authentication',
                    style: TextStyle(color: Colors.white),
                  ),
                ),
                body: Container(
                  color: Colors.red,
                  child: const Center(
                    child: Text("Something wrong"),
                  ),
                ),
              );
            }
          },
        ),
      ),
    );
  }



  @override
  void dispose() {
    super.dispose();
  }
}
