import 'dart:convert';
import 'dart:typed_data';

import 'package:dio/dio.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:photo_album_client/models/photo_body.dart';
import 'package:photo_album_client/services/di/di_utils.dart';

import '../bloc/interactor.dart';
import '../bloc/photo/photo_bloc.dart';
import '../main.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({
    super.key,
  });

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  final _interactor = injector<Interactor>();
  bool order = false;
  List<PhotoBody> photos = [];

  int sortFunction(PhotoBody a, PhotoBody b) {
    if (order) {
      return b.date.compareTo(a.date);
    } else {
      return a.name.compareTo(b.name);
    }
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
        create: (context) => injector<PhotoBloc>(),
        child: BlocListener<PhotoBloc, PhotoState>(listener: (context, state) {
          if (state is Error) {
            ScaffoldMessenger.of(context).showSnackBar(
              const SnackBar(
                duration: Duration(seconds: 15),
                content: Text("Error occured while getting photos."),
                elevation: 20,
              ),
            );
          }
        }, child: BlocBuilder<PhotoBloc, PhotoState>(
          builder: (context, state) {
            if (state is Loading) {
              BlocProvider.of<PhotoBloc>(context).add(LoadPhotoEvent());
              return Scaffold(
                backgroundColor: Theme.of(context).colorScheme.primaryContainer,
                appBar: AppBar(
                  backgroundColor: Theme.of(context).colorScheme.primary,
                  title: const Text(
                    "Photo album",
                    style: TextStyle(color: Colors.white),
                  ),
                  actions: [
                    IconButton(
                      onPressed: () {
                        injector<Interactor>().token = '';
                        navigatorKey.currentState!.pushReplacementNamed('/auth');
                      },
                      icon: const Icon(
                        Icons.logout,
                        size: 28,
                        color: Colors.white,
                      ),
                    ),
                  ],
                ),
                body: const Center(
                  child: CircularProgressIndicator(
                    color: Colors.white38,
                  ),
                ),
              );
            }
            if (state is Content) {
              photos = state.photos;
              photos.sort((a, b) => sortFunction(a, b));
              return Scaffold(
                backgroundColor: Theme.of(context).colorScheme.primaryContainer,
                appBar: AppBar(
                  backgroundColor: Theme.of(context).colorScheme.primary,
                  title: const Text(
                    "Photo album",
                    style: TextStyle(color: Colors.white),
                  ),
                  actions: [
                    IconButton(
                      onPressed: () {
                        setState(() {
                          order = !order;
                        });
                      },
                      icon: const Icon(
                        Icons.sort,
                        size: 28,
                        color: Colors.white,
                      ),
                    ),
                    IconButton(
                      onPressed: () {
                        injector<Interactor>().token = '';
                        navigatorKey.currentState!.pushReplacementNamed('/auth');
                      },
                      icon: const Icon(
                        Icons.logout,
                        size: 28,
                        color: Colors.white,
                      ),
                    ),
                  ],
                ),
                body: photos.isNotEmpty
                    ? ListView.builder(
                        itemCount: photos.length,
                        itemBuilder: (context, index) {
                          return Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 15),
                            child: Card(
                              elevation: 20,
                              color: Colors.white,
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(12.0),
                              ),
                              child: ListTile(
                                leading: Container(
                                  decoration: BoxDecoration(
                                    borderRadius: const BorderRadius.all(Radius.circular(12.0)),
                                    color: Theme.of(context).colorScheme.primary,
                                  ),
                                  child: IconButton(
                                    //color: Theme.of(context).colorScheme.primary,
                                    onPressed: () {
                                      showDialog(
                                        context: context,
                                        barrierDismissible: true, // user must tap button!
                                        builder: (BuildContext context) {
                                          return AlertDialog(
                                            content: ClipRRect(
                                              borderRadius: BorderRadius.circular(12.0),
                                              child: Image.network(
                                                'https://rust-production.up.railway.app/api/photos/${photos[index].id}',
                                                headers: {
                                                  'Authorization': "Bearer ${_interactor.token}",
                                                },
                                              ),
                                            ),
                                          );
                                        }
                                      );
                                    },
                                    iconSize: 28,
                                    icon: const Icon(
                                      Icons.picture_in_picture_rounded,
                                      color: Colors.white,
                                    ),
                                  ),
                                ),
                                trailing: IconButton(
                                  icon: const Icon(Icons.delete),
                                  onPressed: () async {
                                    await _interactor.deletePhoto(photos[index].id);
                                    BlocProvider.of<PhotoBloc>(context).add(LoadPhotoEvent());
                                  },
                                ),
                                style: ListTileStyle.list,
                                title: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisSize: MainAxisSize.min,
                                  children: [
                                    Text(
                                      photos[index].name,
                                      style: const TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                                    ),
                                    Text(
                                      photos[index].id,
                                      style: const TextStyle(fontSize: 16, fontStyle: FontStyle.italic),
                                    ),
                                    Text(
                                      photos[index].date.toString(),
                                      style: const TextStyle(fontSize: 18, fontWeight: FontWeight.normal),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          );
                        },
                      )
                    : const Center(
                        child: Text(
                          "There is no photos up here, try upload your own photo",
                          style: TextStyle(fontSize: 20),
                        ),
                      ),
                floatingActionButton: FloatingActionButton(
                  onPressed: () async {
                    /// Upload Photo
                    FilePickerResult? result = await FilePicker.platform.pickFiles(
                      type: FileType.custom,
                      allowedExtensions: ['jpg', 'jpeg', 'png'],
                      withData: true,
                    );
                    if (result != null) {
                      final fileBytes = result.files.first.bytes;
                      final fileName = result.files.first.name;
                      if (fileBytes != null) {
                        var image = MultipartFile.fromBytes(fileBytes as List<int>, filename: fileName);
                        PhotoBody res = await _interactor.postPhoto(image);
                        BlocProvider.of<PhotoBloc>(context).add(LoadPhotoEvent());
                      }
                    }
                  },
                  backgroundColor: Theme.of(context).colorScheme.primary,
                  child: const Icon(
                    Icons.add,
                    size: 28,
                    color: Colors.white,
                  ),
                ),
              );
            }
            return Scaffold(
              backgroundColor: Theme.of(context).colorScheme.primaryContainer,
              appBar: AppBar(
                backgroundColor: Theme.of(context).colorScheme.primary,
                title: const Text(
                  "Photo album",
                  style: TextStyle(color: Colors.white),
                ),
                actions: [
                  IconButton(
                    onPressed: () {
                      injector<Interactor>().token = '';
                      navigatorKey.currentState!.pushReplacementNamed('/auth');
                    },
                    icon: const Icon(
                      Icons.logout,
                      size: 28,
                      color: Colors.white,
                    ),
                  ),
                ],
              ),
              body: const Center(
                child: Text("Something wrong here"),
              ),
            );
          },
        )));
  }
}
