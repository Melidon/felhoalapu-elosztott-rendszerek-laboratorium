import 'package:json_annotation/json_annotation.dart';

part 'auth_payload.g.dart';

@JsonSerializable(fieldRename: FieldRename.snake)
final class AuthPayload {
  final String username;
  final String password;

  const AuthPayload({
    required this.username,
    required this.password,
  });

  factory AuthPayload.fromJson(Map<String, dynamic> json) => _$AuthPayloadFromJson(json);
  Map<String, dynamic> toJson() => _$AuthPayloadToJson(this);
}
