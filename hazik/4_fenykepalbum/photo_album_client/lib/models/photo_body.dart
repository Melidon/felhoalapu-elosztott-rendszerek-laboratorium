import 'package:json_annotation/json_annotation.dart';

part 'photo_body.g.dart';

@JsonSerializable(fieldRename: FieldRename.snake)
final class PhotoBody {
  final String id;
  final String name;
  final DateTime date;

  const PhotoBody({
    required this.id,
    required this.name,
    required this.date,
  });

  factory PhotoBody.fromJson(Map<String, dynamic> json) => _$PhotoBodyFromJson(json);
  Map<String, dynamic> toJson() => _$PhotoBodyToJson(this);
}
