import 'package:json_annotation/json_annotation.dart';

part 'auth_body.g.dart';

@JsonSerializable(fieldRename: FieldRename.snake)
final class AuthBody {
  final String accessToken;
  final String tokenType;

  const AuthBody({
    required this.accessToken,
    required this.tokenType,
  });

  factory AuthBody.fromJson(Map<String, dynamic> json) => _$AuthBodyFromJson(json);
  Map<String, dynamic> toJson() => _$AuthBodyToJson(this);
}
