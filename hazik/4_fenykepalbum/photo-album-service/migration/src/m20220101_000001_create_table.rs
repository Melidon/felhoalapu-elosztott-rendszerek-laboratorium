use sea_orm_migration::prelude::*;

#[derive(DeriveMigrationName)]
pub struct Migration;

#[async_trait::async_trait]
impl MigrationTrait for Migration {
    async fn up(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        manager
            .create_table(
                Table::create()
                    .table(User::Table)
                    .if_not_exists()
                    .col(ColumnDef::new(User::Id).uuid().not_null().primary_key())
                    .col(
                        ColumnDef::new(User::Username)
                            .string()
                            .not_null()
                            .unique_key(),
                    )
                    .col(ColumnDef::new(User::PasswordHash).string().not_null())
                    .to_owned(),
            )
            .await?;
        manager
            .create_table(
                Table::create()
                    .table(Photo::Table)
                    .if_not_exists()
                    .col(ColumnDef::new(Photo::Id).uuid().not_null().primary_key())
                    .col(ColumnDef::new(Photo::UserId).uuid().not_null())
                    .foreign_key(
                        ForeignKey::create()
                            .name("fk_user_id")
                            .from(Photo::Table, Photo::UserId)
                            .to(User::Table, User::Id),
                    )
                    .col(ColumnDef::new(Photo::Name).string().not_null())
                    .col(ColumnDef::new(Photo::Date).date_time().not_null())
                    .col(ColumnDef::new(Photo::Bytes).binary().not_null())
                    .to_owned(),
            )
            .await?;
        Ok(())
    }

    async fn down(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        manager
            .drop_table(Table::drop().table(Photo::Table).to_owned())
            .await?;
        manager
            .drop_table(Table::drop().table(User::Table).to_owned())
            .await?;
        Ok(())
    }
}

#[derive(DeriveIden)]
pub(crate) enum User {
    Table,
    Id,
    Username,
    PasswordHash,
}

#[derive(DeriveIden)]
enum Photo {
    Table,
    Id,
    UserId,
    Name,
    Date,
    Bytes,
}
