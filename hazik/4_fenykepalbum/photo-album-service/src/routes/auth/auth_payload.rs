use serde::Deserialize;

#[derive(Deserialize)]
pub(crate) struct AuthPayload {
    pub(super) username: String,
    pub(super) password: String,
}
