use {
    axum::{
        http::StatusCode,
        response::{IntoResponse, Response},
        Json,
    },
    serde_json::json,
};

pub(crate) enum AuthError {
    Hash,
    Database,
    TokenCreation,
    WrongUsername,
    WrongPassword,
    InvalidToken,
    ExpiredToken,
}

impl IntoResponse for AuthError {
    fn into_response(self) -> Response {
        let (status, error_message) = match self {
            AuthError::Hash => (StatusCode::INTERNAL_SERVER_ERROR, "Hash error"),
            AuthError::Database => (StatusCode::INTERNAL_SERVER_ERROR, "Database error"),
            AuthError::TokenCreation => (StatusCode::INTERNAL_SERVER_ERROR, "Token creation error"),
            AuthError::WrongUsername => (StatusCode::UNAUTHORIZED, "Wrong username"),
            AuthError::WrongPassword => (StatusCode::UNAUTHORIZED, "Wrong password"),
            AuthError::InvalidToken => (StatusCode::UNAUTHORIZED, "Invalid token"),
            AuthError::ExpiredToken => (StatusCode::UNAUTHORIZED, "Expired token"),
        };
        let body = Json(json!({
            "error": error_message,
        }));
        (status, body).into_response()
    }
}
