use {
    super::{auth_error::AuthError, keys::KEYS},
    axum::{async_trait, extract::FromRequestParts, http::request::Parts, RequestPartsExt},
    axum_extra::{
        headers::{authorization::Bearer, Authorization},
        TypedHeader,
    },
    chrono::{Duration, Utc},
    jsonwebtoken::{decode, Validation},
    serde::{Deserialize, Serialize},
    uuid::Uuid,
};

#[derive(Serialize, Deserialize)]
pub(crate) struct Claims {
    pub exp: i64,
    pub(crate) user_id: Uuid,
}

impl Claims {
    pub(super) fn new(user_id: Uuid) -> Self {
        Self {
            exp: (Utc::now() + Duration::days(1)).timestamp(),
            user_id,
        }
    }
}

#[async_trait]
impl<S> FromRequestParts<S> for Claims
where
    S: Send + Sync,
{
    type Rejection = AuthError;

    async fn from_request_parts(parts: &mut Parts, _state: &S) -> Result<Self, Self::Rejection> {
        let TypedHeader(Authorization(bearer)) = parts
            .extract::<TypedHeader<Authorization<Bearer>>>()
            .await
            .map_err(|_| AuthError::InvalidToken)?;
        let claims = decode::<Claims>(bearer.token(), &KEYS.decoding, &Validation::default())
            .map_err(|_| AuthError::InvalidToken)?
            .claims;
        if claims.exp < Utc::now().timestamp() {
            return Err(AuthError::ExpiredToken);
        }
        Ok(claims)
    }
}
