mod auth_body;
mod auth_error;
mod auth_payload;
pub(super) mod claims;
mod keys;

use {
    self::{
        auth_body::AuthBody, auth_error::AuthError, auth_payload::AuthPayload, claims::Claims,
        keys::KEYS,
    },
    crate::entities::{
        prelude::User as UserTable,
        user::{ActiveModel as User, Column::Username},
    },
    axum::{http::StatusCode, Extension, Json},
    jsonwebtoken::Header,
    sea_orm::{ColumnTrait, DatabaseConnection, EntityTrait, QueryFilter, Set},
    uuid::Uuid,
};

pub(crate) async fn sign_in(
    Extension(db): Extension<DatabaseConnection>,
    Json(payload): Json<AuthPayload>,
) -> Result<Json<AuthBody>, AuthError> {
    let db_user = UserTable::find()
        .filter(Username.eq(payload.username))
        .one(&db)
        .await
        .map_err(|_| AuthError::Database)?
        .ok_or(AuthError::WrongUsername)?;
    if !bcrypt::verify(payload.password, &db_user.password_hash).map_err(|_| AuthError::Hash)? {
        return Err(AuthError::WrongPassword);
    }
    let token = create_token(db_user.id)?;
    Ok(Json(AuthBody::new(token)))
}

pub(crate) async fn sign_up(
    Extension(db): Extension<DatabaseConnection>,
    Json(payload): Json<AuthPayload>,
) -> Result<(StatusCode, Json<AuthBody>), AuthError> {
    let password_hash =
        bcrypt::hash(payload.password, bcrypt::DEFAULT_COST).map_err(|_| AuthError::Hash)?;
    let user = User {
        id: Set(Uuid::new_v4()),
        username: Set(payload.username),
        password_hash: Set(password_hash),
    };
    let db_user = UserTable::insert(user)
        .exec_with_returning(&db)
        .await
        .map_err(|_| AuthError::Database)?;
    let token = create_token(db_user.id)?;
    Ok((StatusCode::CREATED, Json(AuthBody::new(token))))
}

fn create_token(user_id: Uuid) -> Result<String, AuthError> {
    let claims = Claims::new(user_id);
    jsonwebtoken::encode(&Header::default(), &claims, &KEYS.encoding)
        .map_err(|_| AuthError::TokenCreation)
}
