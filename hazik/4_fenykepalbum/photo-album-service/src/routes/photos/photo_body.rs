use {crate::entities::photo::Model as Photo, chrono::NaiveDateTime, serde::Serialize, uuid::Uuid};

#[derive(Serialize)]
pub(crate) struct PhotoBody {
    pub(super) id: Uuid,
    pub(super) name: String,
    pub(super) date: NaiveDateTime,
}

impl From<Photo> for PhotoBody {
    fn from(photo: Photo) -> Self {
        Self {
            id: photo.id,
            name: photo.name,
            date: photo.date,
        }
    }
}
