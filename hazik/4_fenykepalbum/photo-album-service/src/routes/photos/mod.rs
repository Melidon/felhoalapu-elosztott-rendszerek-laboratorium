mod photo_body;
mod photo_error;

use {
    self::{photo_body::PhotoBody, photo_error::PhotoError},
    super::auth::claims::Claims,
    crate::entities::{
        photo::{
            ActiveModel as Photo,
            Column::{Id, UserId},
        },
        prelude::Photo as PhotoTable,
    },
    axum::{
        extract::{Multipart, Path},
        http::{header::CONTENT_TYPE, HeaderName, HeaderValue, StatusCode},
        Extension, Json,
    },
    chrono::Utc,
    image::ImageFormat,
    sea_orm::{ColumnTrait, DatabaseConnection, EntityTrait, QueryFilter, Set},
    uuid::Uuid,
};

const SUPPORTED_FORMATS: [ImageFormat; 2] = [ImageFormat::Jpeg, ImageFormat::Png];

pub(crate) async fn upload(
    Extension(db): Extension<DatabaseConnection>,
    claims: Claims,
    mut multipart: Multipart,
) -> Result<(StatusCode, Json<PhotoBody>), PhotoError> {
    let field = multipart
        .next_field()
        .await
        .map_err(PhotoError::MultipartError)?
        .ok_or(PhotoError::NoFile)?;
    let name = field.name().ok_or(PhotoError::MissingName)?.to_string();
    let bytes = field
        .bytes()
        .await
        .map_err(PhotoError::MultipartError)?
        .to_vec();
    let image_format = image::guess_format(&bytes).map_err(PhotoError::ImageError)?;
    SUPPORTED_FORMATS
        .contains(&image_format)
        .then_some(())
        .ok_or(PhotoError::InvalidFormat)?;
    let photo = Photo {
        id: Set(Uuid::new_v4()),
        user_id: Set(claims.user_id),
        name: Set(name),
        date: Set(Utc::now().naive_utc()),
        bytes: Set(bytes),
    };
    let db_photo = PhotoTable::insert(photo)
        .exec_with_returning(&db)
        .await
        .map_err(|_| PhotoError::Database)?;
    let photo_body = PhotoBody {
        id: db_photo.id,
        name: db_photo.name,
        date: db_photo.date,
    };
    Ok((StatusCode::CREATED, Json(photo_body)))
}

pub(crate) async fn list(
    Extension(db): Extension<DatabaseConnection>,
    claims: Claims,
) -> Result<Json<Vec<PhotoBody>>, PhotoError> {
    let photos = PhotoTable::find()
        .filter(UserId.eq(claims.user_id))
        .all(&db)
        .await
        .map_err(|_| PhotoError::Database)?
        .into_iter()
        .map(PhotoBody::from)
        .collect();
    Ok(Json(photos))
}

pub(crate) async fn get(
    Extension(db): Extension<DatabaseConnection>,
    claims: Claims,
    Path(id): Path<Uuid>,
) -> Result<([(HeaderName, HeaderValue); 1], Vec<u8>), PhotoError> {
    let photo = PhotoTable::find()
        .filter(Id.eq(id).and(UserId.eq(claims.user_id)))
        .one(&db)
        .await
        .map_err(|_| PhotoError::Database)?
        .ok_or(PhotoError::NotFound)?
        .bytes;
    let content_type = image::guess_format(&photo)
        .unwrap()
        .to_mime_type()
        .try_into()
        .unwrap();
    Ok(([(CONTENT_TYPE, content_type)], photo))
}

pub(crate) async fn delete(
    Extension(db): Extension<DatabaseConnection>,
    claims: Claims,
    Path(id): Path<Uuid>,
) -> Result<StatusCode, PhotoError> {
    PhotoTable::delete_many()
        .filter(Id.eq(id).and(UserId.eq(claims.user_id)))
        .exec(&db)
        .await
        .map_err(|_| PhotoError::Database)?;
    Ok(StatusCode::NO_CONTENT)
}
