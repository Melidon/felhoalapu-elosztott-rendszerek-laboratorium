use {
    axum::{
        extract::multipart::MultipartError,
        http::StatusCode,
        response::{IntoResponse, Response},
        Json,
    },
    image::ImageError,
    serde_json::json,
};

pub(crate) enum PhotoError {
    MultipartError(MultipartError),
    NoFile,
    MissingName,
    ImageError(ImageError),
    InvalidFormat,
    Database,
    NotFound,
}

impl IntoResponse for PhotoError {
    fn into_response(self) -> Response {
        let (status, error_message) = match self {
            PhotoError::MultipartError(multipart_error) => {
                (multipart_error.status(), multipart_error.to_string())
            }
            PhotoError::NoFile => (StatusCode::BAD_REQUEST, "No file".to_owned()),
            PhotoError::MissingName => (StatusCode::BAD_REQUEST, "Missing name".to_owned()),
            PhotoError::ImageError(image_error) => {
                (StatusCode::BAD_REQUEST, image_error.to_string())
            }
            PhotoError::InvalidFormat => (
                StatusCode::BAD_REQUEST,
                "Only Jpeg and Png formats are supported".to_owned(),
            ),
            PhotoError::Database => (
                StatusCode::INTERNAL_SERVER_ERROR,
                "Database error".to_owned(),
            ),
            PhotoError::NotFound => (StatusCode::NOT_FOUND, "Photo not found".to_owned()),
        };
        let body = Json(json!({
            "error": error_message,
        }));
        (status, body).into_response()
    }
}
