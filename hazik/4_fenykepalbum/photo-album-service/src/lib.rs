mod entities;
mod routes;

use {
    axum::{
        routing::{delete, get, post},
        Extension, Router,
    },
    routes::{auth, photos},
    sea_orm::DatabaseConnection,
    tower_http::cors::CorsLayer,
};

pub fn get_app(db: DatabaseConnection) -> Router {
    Router::new()
        .nest(
            "/api",
            Router::new()
                .nest(
                    "/auth",
                    Router::new()
                        .route("/sign-in", post(auth::sign_in))
                        .route("/sign-up", post(auth::sign_up)),
                )
                .nest(
                    "/photos",
                    Router::new()
                        .route("/", post(photos::upload))
                        .route("/", get(photos::list))
                        .route("/:id", get(photos::get))
                        .route("/:id", delete(photos::delete)),
                )
                .layer(Extension(db)),
        )
        // TODO: Remove this in production
        .layer(CorsLayer::very_permissive())
}
