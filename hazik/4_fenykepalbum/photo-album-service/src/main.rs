use {
    migration::{Migrator, MigratorTrait},
    sea_orm::Database,
    std::{
        env,
        net::{Ipv4Addr, SocketAddrV4},
    },
    tokio::net::TcpListener,
};

#[allow(clippy::expect_fun_call)]
#[tokio::main]
async fn main() {
    dotenvy::dotenv().ok();
    let database_url = env::var("DATABASE_URL").expect("DATABASE_URL is not set");
    let db = Database::connect(&database_url)
        .await
        .expect(&format!("Failed to connect to: {database_url}"));
    Migrator::up(&db, None)
        .await
        .expect("Failed to run migrations");
    let app: axum::Router = photo_album_service::get_app(db);
    let port = env::var("PORT").map_or(3000, |port| port.parse().unwrap());
    let listener = TcpListener::bind(SocketAddrV4::new(Ipv4Addr::UNSPECIFIED, port))
        .await
        .expect(&format!("Failed to bind to port: {port}"));
    axum::serve(listener, app).await.unwrap();
}
