#include <algorithm>
#include <array>
#include <chrono>
#include <complex>
#include <cstdint>
#include <filesystem>
#include <fstream>
#include <iostream>
#include <vector>

#include <mpi.h>

using color = std::array<uint8_t, 3>;

auto WriteTGA_RGB(const std::filesystem::path& filename, color *data, size_t width, size_t height) -> void {
    using std::ios;

    auto filebuf = std::filebuf{};
    filebuf.open(filename, ios::out | ios::binary);

    filebuf.sputc(0x00); // ID Length, 0 => No ID
    filebuf.sputc(0x00); // Color Map Type, 0 => No color map included
    filebuf.sputc(0x02); // Image Type, 2 => Uncompressed, True-color Image
    filebuf.sputc(0x00); // Next five bytes are about the color map entries
    filebuf.sputc(0x00); // 2 bytes Index, 2 bytes length, 1 byte size
    filebuf.sputc(0x00);
    filebuf.sputc(0x00);
    filebuf.sputc(0x00);
    filebuf.sputc(0x00); // X-origin of Image
    filebuf.sputc(0x00);
    filebuf.sputc(0x00); // Y-origin of Image
    filebuf.sputc(0x00);
    filebuf.sputc(static_cast<char>(width & 0xff)); // Image Width
    filebuf.sputc(static_cast<char>((width >> 8) & 0xff));
    filebuf.sputc(static_cast<char>(height & 0xff)); // Image Height
    filebuf.sputc(static_cast<char>((height >> 8) & 0xff));
    filebuf.sputc(0x18); // Pixel Depth, 0x18 => 24 Bits
    filebuf.sputc(0x20); // Image Descriptor

    filebuf.sputn(reinterpret_cast<char *>(data), static_cast<std::streamsize>(width * height * sizeof(color)));
}

auto coloring_needed(double r, double i) -> bool {
    using complex = std::complex<double>;

    constexpr double scale = 2.0;
    constexpr auto center = complex{-1.25, -1.0};
    constexpr size_t maxIterations = 256;

    const auto c = complex{r * scale + center.real(), i * scale + center.imag()};
    auto z = c;

    for (size_t i = 0; i < maxIterations; ++i) {
        z = z * z + c;
        if (abs(z) > 1.0) {
            return true;
        }
    }
    return false;
}

auto calculate(color *data, size_t width, size_t height) -> void {
    int _size;
    MPI_Comm_size(MPI_COMM_WORLD, &_size);
    const size_t size = static_cast<size_t>(_size);
    int _rank;
    MPI_Comm_rank(MPI_COMM_WORLD, &_rank);
    const size_t rank = static_cast<size_t>(_rank);

    for (size_t i = 0; i < size; ++i) {
        size_t begin = height * i / size;
        size_t end = height * (i + 1) / size;
        if (rank == i) {
#pragma omp parallel for schedule(static)
            for (size_t y = begin; y < end; ++y) {
                for (size_t x = 0; x < width; ++x) {
                    if (coloring_needed(static_cast<double>(x) / static_cast<double>(width), static_cast<double>(y) / static_cast<double>(height))) {
                        data[x + y * width][0] = 255; // blue
                        data[x + y * width][1] = 255; // green
                        data[x + y * width][2] = 255; // red
                    }
                }
            }
            if (i > 0) {
                MPI_Send(data + begin * width, static_cast<int>((end - begin) * width * sizeof(color)), MPI_BYTE, 0, 0, MPI_COMM_WORLD);
            }
        }
        if (i > 0 && rank == 0) {
            MPI_Status status;
            MPI_Recv(data + begin * width, static_cast<int>((end - begin) * width * sizeof(color)), MPI_BYTE, static_cast<int>(i), 0, MPI_COMM_WORLD, &status);
        }
    }
}

auto main(int argc, char **argv) -> int {
    using namespace std::chrono;

    constexpr size_t domainWidth = 1024;
    constexpr size_t domainHeight = 1024;
    auto data = std::vector<color>{domainWidth * domainHeight};

    MPI_Init(&argc, &argv);

    const auto begin = high_resolution_clock::now();
    calculate(data.data(), domainWidth, domainHeight);
    const auto end = high_resolution_clock::now();

    int rank;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Finalize();
    if (rank == 0) {
        const auto elapsed = duration_cast<milliseconds>(end - begin);
        std::cout << "Elapsed time: " << elapsed.count() << "ms" << std::endl;
        WriteTGA_RGB("mandelbrot.tga", data.data(), domainWidth, domainHeight);
    }
    return EXIT_SUCCESS;
}
