#  Fraktálhalmazok (Feladat #1) -> 1_FRACTAL

## Bálint Gergő (O78UXU), Szommer Zsombor (MM5NOT)

Cél egy fraktálgenerátor párhuzamosított változatának megvalósítása.

### Beadott file-ok:

- **Ez a dokumentáció**
- **main.cpp**
- **run.sh**
- **Makefile**

### Feladat kivitelezése

A kapott soros végrehajtású programot először átstruktúráltuk. Függvényekbe szerveztük ki az arra érdemes részeket, és modern c++ elemekre cseréltünk mindent amit lehetett. 

A megoldásunk **OpenMP**-t tartalmaz a szálkezelés egyszerűsítésére a gyorsításhoz, illetve **MPI**-t a példánytöbbszörösítésre. A megoldást **Slurm** segítségével szétosztva tudjuk futtatni a szervergép node-okon.

Feladat az elért gyorsulás kimérése, és dokumentálása. Az időmérést `std::chrono::high_resolution_clock` segítségével végeztük a head node-on. Különböző paraméterek változtatásával kimértük a futási időket a para szerveren, mérési eredményeink a következő táblázatban láthatók, az iterációszámot felemeltük 256-ra:

| Size        |     Basic |   OpenMP |      MPI | OpenMP + MPI |
| ----------- | --------: | -------: | -------: | -----------: |
| 1024*1024   |    8.126s |   3.426s |   0.851s |       0.772s |
| 2048*2048   |   32.256s |  13.514s |   3.442s |       2.957s |
| 4096*4096   |  128.163s |  54.232s |  13.358s |      11.675s |
| 8192*8192   |  517.142s | 215.843s |  53.765s |      46.430s |
| 16384*16384 | 2047.235s | 859.229s | 216.010s |     186.042s |


Amikor csak MPI-t használunk, akkor érdekes, hogy a 4 node (és -N 4 kapcsoló) ellenére 16 példány indul. Ezzel magyarázható a 10-szeres gyorsulás a basic-hez képest, illetve hogy az MPI és az OpenMP + MPI esetek között nincs nagy különbség.

A mérési eredményekből a következő jelleggörbéket kapjuk logaritmikus skálán, ezen jól látható a különböző megoldások jósága egymáshoz képest:

![Diagram](images/diagram.png)

Ezen pedig lineárisan skálázva látszik a futásidők növekedése a képméret függvényében:

![Diagram_linear](images/diagram_lin.png)

Generált nagyfelbontású Mandelbrot halmaz:

![Mandelbrot](images/mandelbrot.png)
