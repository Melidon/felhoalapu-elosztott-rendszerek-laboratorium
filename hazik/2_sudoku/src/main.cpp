#include <chrono>
#include <iostream>
#include <vector>

#include <mpi.h>

#include "solver.h"

auto get_rank(MPI_Comm comm) -> std::size_t {
    int rank;
    MPI_Comm_rank(comm, &rank);
    return static_cast<std::size_t>(rank);
}

auto get_size(MPI_Comm comm) -> std::size_t {
    int size;
    MPI_Comm_size(comm, &size);
    return static_cast<std::size_t>(size);
}

template <typename T>
auto create_type() -> MPI_Datatype {
    MPI_Datatype type;
    MPI_Type_contiguous(sizeof(T), MPI_BYTE, &type);
    MPI_Type_commit(&type);
    return type;
}

auto get_puzzles(int argc, char **argv) -> std::vector<std::string_view> {
    auto puzzles = std::vector<std::string_view>{};
    if (argc == 1) {
        // other test puzzles can be found at
        // https://github.com/grkuntzmd/sudoku/tree/master/test_puzzles
        auto test = "...8.1..........435............7.8...2..3..........1..6......75..34........2..6..";
        puzzles.push_back(test);
    } else {
        puzzles.reserve(static_cast<std::size_t>(argc) - 1);
        for (int i = 1; i < argc; i++) {
            puzzles.push_back(argv[i]);
        }
    }
    return puzzles;
}

auto get_counts(std::size_t total_count, std::size_t size) -> std::vector<int> {
    auto counts = std::vector<int>(size);
    for (std::size_t i = 0; i < size; i++) {
        std::size_t begin = i * total_count / size;
        std::size_t end = (i + 1) * total_count / size;
        counts[i] = static_cast<int>(end - begin);
    }
    return counts;
}

auto get_placements(const std::vector<int> &counts) -> std::vector<int> {
    auto placements = std::vector<int>(counts.size());
    placements[0] = 0;
    for (std::size_t i = 1; i < counts.size(); i++) {
        placements[i] = placements[i - 1] + counts[i - 1];
    }
    return placements;
}

auto main(int argc, char **argv) -> int {
    using namespace std::chrono;

    MPI_Init(&argc, &argv);

    const auto rank = get_rank(MPI_COMM_WORLD);
    const auto size = get_size(MPI_COMM_WORLD);

    auto MPI_SUDOKU = create_type<Sudoku>();

    const auto start = high_resolution_clock::now();

    const auto puzzles = get_puzzles(argc, argv);
    const auto counts = get_counts(puzzles.size(), size);
    const auto placements = get_placements(counts);
    auto some = std::vector<Sudoku>(static_cast<std::size_t>(counts[rank]));

    if (rank == 0) {
        auto all = std::vector<Sudoku>{};
        all.reserve(puzzles.size());
        for (auto puzzle : puzzles) {
            all.emplace_back(puzzle);
        }
        MPI_Scatterv(all.data(), counts.data(), placements.data(), MPI_SUDOKU, some.data(), counts[rank], MPI_SUDOKU, 0, MPI_COMM_WORLD);
    } else {
        MPI_Scatterv(nullptr, nullptr, nullptr, MPI_SUDOKU, some.data(), counts[rank], MPI_SUDOKU, 0, MPI_COMM_WORLD);
    }

#pragma omp parallel for schedule(static)
    for (std::size_t i = 0; i < some.size(); ++i) {
        if (!solve(some[i])) {
            std::cout << "Could not solve:\n" << some[i] << std::endl;
        }
    }

    if (rank != 0) {
        MPI_Gatherv(some.data(), counts[rank], MPI_SUDOKU, nullptr, nullptr, nullptr, MPI_SUDOKU, 0, MPI_COMM_WORLD);
    } else {
        auto all = std::vector<Sudoku>(puzzles.size());
        MPI_Gatherv(some.data(), counts[rank], MPI_SUDOKU, all.data(), counts.data(), placements.data(), MPI_SUDOKU, 0, MPI_COMM_WORLD);
        const auto end = high_resolution_clock::now();
        std::cout << "Elapsed time: " << duration_cast<milliseconds>(end - start).count() << " ms" << std::endl;
        for (const auto &sudoku : all) {
            std::cout << sudoku << std::endl;
        }
    }

    MPI_Type_free(&MPI_SUDOKU);
    MPI_Finalize();

    return EXIT_SUCCESS;
}
