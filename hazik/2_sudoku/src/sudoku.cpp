#include "sudoku.h"

#include <stdexcept>

Sudoku::Sudoku() {
    for (std::size_t row = 0; row < 9; ++row) {
        for (std::size_t col = 0; col < 9; ++col) {
            grid[row][col] = Note{}.set();
        }
    }
}

Sudoku::Sudoku(std::string_view puzzle) : Sudoku{} {
    if (puzzle.size() != 81) {
        throw std::invalid_argument("Puzzle must contain 81 characters");
    }
    for (std::size_t i = 0; i < 81; ++i) {
        const std::size_t row = i / 9;
        const std::size_t col = i % 9;
        if (puzzle[i] == '.') {
            continue;
        } else if (puzzle[i] >= '1' && puzzle[i] <= '9') {
            try {
                this->set(row, col, static_cast<Digit>(puzzle[i] - '0'));
            } catch (std::invalid_argument &) {
                throw std::invalid_argument("Puzzle is invalid");
            }
        } else {
            throw std::invalid_argument("Puzzle must contain only digits from 1 to 9 or '.'");
        }
    }
}

std::ostream &operator<<(std::ostream &os, const Sudoku &sudoku) {
    for (std::size_t row = 0; row < 9; ++row) {
        if (row % 3 == 0) {
            os << "-------------\n";
        }
        for (std::size_t col = 0; col < 9; ++col) {
            if (col % 3 == 0) {
                os << '|';
            }
            if (const auto digit = std::get_if<Sudoku::Digit>(&sudoku.grid[row][col]); digit != nullptr) {
                os << static_cast<std::size_t>(*digit);
            } else {
                os << '.';
            }
        }
        os << "|\n";
    }
    os << "-------------\n";
    return os;
}

[[nodiscard]] const Sudoku::Cell &Sudoku::get(std::size_t row, std::size_t col) const noexcept {
    return grid[row][col];
}

void Sudoku::set(std::size_t row, std::size_t col, Digit digit) {
    if (std::holds_alternative<Digit>(grid[row][col])) {
        throw std::invalid_argument("Cell already contains a digit.");
    }
    if (const auto &note = std::get<Note>(grid[row][col]); !note[digit - 1]) {
        throw std::invalid_argument("Cell does not contain the digit in its note.");
    }
    grid[row][col] = digit;
    for (std::size_t i = 0; i < 9; ++i) {
        if (auto note = std::get_if<Note>(&grid[row][i]); note != nullptr) {
            (*note)[digit - 1] = false;
        }
        if (auto note = std::get_if<Note>(&grid[i][col]); note != nullptr) {
            (*note)[digit - 1] = false;
        }
    }
    const std::size_t subgrid_row_start = row / 3 * 3;
    const std::size_t subgrid_col_start = col / 3 * 3;
    for (std::size_t i = subgrid_row_start; i < subgrid_row_start + 3; ++i) {
        for (std::size_t j = subgrid_col_start; j < subgrid_col_start + 3; ++j) {
            if (auto note = std::get_if<Note>(&grid[i][j]); note != nullptr) {
                (*note)[digit - 1] = false;
            }
        }
    }
}
