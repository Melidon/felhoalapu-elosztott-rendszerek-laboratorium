#pragma once

#include <array>
#include <bitset>
#include <cstdint>
#include <iostream>
#include <string_view>
#include <variant>

class Sudoku {
public:
    using Digit = std::uint8_t;
    using Note = std::bitset<9>;
    using Cell = std::variant<Digit, Note>;

private:
    template <typename T, std::size_t HEIGHT, std::size_t WIDTH>
    using matrix = std::array<std::array<T, WIDTH>, HEIGHT>;

    matrix<Cell, 9, 9> grid;

public:
    explicit Sudoku();
    explicit Sudoku(std::string_view puzzle);

    friend std::ostream &operator<<(std::ostream &os, const Sudoku &sudoku);

    [[nodiscard]] const Cell &get(std::size_t row, std::size_t col) const noexcept;
    void set(std::size_t row, std::size_t col, Digit digit);
};
