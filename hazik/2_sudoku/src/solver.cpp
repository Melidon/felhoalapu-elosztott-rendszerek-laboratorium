#include "solver.h"

[[nodiscard]] bool solve(Sudoku &sudoku) noexcept {
    for (std::size_t count = 1; count <= 9; ++count) {
        for (std::size_t row = 0; row < 9; ++row) {
            for (std::size_t col = 0; col < 9; ++col) {
                if (const auto note = std::get_if<Sudoku::Note>(&sudoku.get(row, col)); note != nullptr) {
                    if (note->count() > count) {
                        continue;
                    }
                    for (Sudoku::Digit digit = 1; digit <= 9; ++digit) {
                        if ((*note)[digit - 1]) {
                            auto copy = sudoku;
                            sudoku.set(row, col, digit);
                            if (solve(sudoku)) {
                                return true;
                            }
                            sudoku = copy;
                        }
                    }
                    return false;
                }
            }
        }
    }
    return true;
}
