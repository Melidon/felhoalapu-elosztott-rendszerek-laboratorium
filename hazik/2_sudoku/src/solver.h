#pragma once

#include "sudoku.h"

[[nodiscard]] bool solve(Sudoku &sudoku) noexcept;
