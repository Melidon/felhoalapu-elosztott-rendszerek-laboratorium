#  Sudoku solver (Feladat #2) -> 2_sudoku

## Bálint Gergő (O78UXU), Szommer Zsombor (MM5NOT)

Cél egy sudoku párhuzamosított változatának megvalósítása, és az ezzel elért gyorsulás kimérése az eredeti egy szálon futó back-tracking algoritmushoz képest.

## Beadott file-ok:

- **README.md**
- **src/**
  - **main.cpp**
  - **solver.cpp**
  - **solver.h**
  - **sudoku.cpp**
  - **sudoku.h**
- **CMakeLists.txt**
- **komondor.sh**
- **para.sh**
- **run.sh**

## Feladat kivitelezése

A kapott soros végrehajtású programot először átstruktúráltuk.
Függvényekbe szerveztük ki az arra érdemes részeket, és modern C++ elemekre cseréltünk mindent amit lehetett.
Ennek a megoldásnak a **para**, és a **komondor** szervereken is futnia kell, ezt két `*.sh` fájl megadásával választottuk külön (para.sh, komondor.sh).

A kapott algoritmust a következőképp tudtuk gyorsítani, még a párhuzamosítás előtt:
Az eredeti verzió végigmegy az összes mezőn, és bepróbálja az összes lehetséges hiányzó számjegyet, emiatt feleslegesen sok az oda-visszalépés.
A mi verziónk kiegészült azzal, hogy nemcsak számon tartja a lehetséges számjegyeket minden mezőre, de az összes még üres mező közül a "leginkább egyértelművel" folytatja a kitöltést, azaz azzal a mezővel, ahol a lehető legkisebb a még beírható számok halmaza.
Ezzel a módszerrel nagymértékő gyorsulást értünk el, olyannyira, hogy az így felkészített algoritmus LeetCode-on a legjobb 1%-ba került futási idő szerint.

![LeetCode](images/proof.png)

Ezután megvalósítottuk a párhuzamosítást is.
A megoldásunk **OpenMP**-t tartalmaz a szálkezelés egyszerűsítésére a gyorsításhoz, illetve **MPI**-t a példánytöbbszörösítésre.
A megoldást **Slurm** segítségével szétosztva tudjuk futtatni a szervergép node-okon.

Feladat volt az elért gyorsulás kimérése, és dokumentálása is.
Az időmérést `std::chrono::high_resolution_clock` segítségével végeztük a head node-on.
Különböző paraméterek változtatásával kimértük a futási időket a para szerveren és a komondoron, mérési eredményeink a következő táblázatban láthatók:

## Eredmények

### Para

| Fájlnév (db)     |     N=1 |     N=4 | OpenMP + N=1 | OpenMP + N=4 |
| ---------------- | ------: | ------: | -----------: | -----------: |
| easy.sdk (50)    |   12 ms |   39 ms |         6 ms |        35 ms |
| hard.sdk (95)    | 2650 ms | 1535 ms |      1487 ms |       682 ms |
| hardest.sdk (11) |   12 ms |   38 ms |        10 ms |        38 ms |

### Komondor

| Fájlnév (db)     |    N=1 |    N=4 | OpenMP + N=1 | OpenMP + N=4 |
| ---------------- | -----: | -----: | -----------: | -----------: |
| easy.sdk (50)    |   2 ms |   1 ms |         2 ms |         1 ms |
| hard.sdk (95)    | 326 ms | 183 ms |       379 ms |       291 ms |
| hardest.sdk (11) |   1 ms | 210 ms |         2 ms |       143 ms |

## Összegzés

Az eltérő elemszámű rejtvényt tartalmazó fájlok miatt az eredményeket csak soronként szabad értelmezni, a sorok egymáshoz való viszonya nem releváns.
A következő tanulságot tudjuk levonni.
Csak akkor éri meg az OpenMP meg a több Node használata, hogy ha elég nagy a kitűzött feladat, mert különben csak a kommunikációs overheadet növeljük.
Látható, hogy az OpenMP kommunikációs overheadje jóval kisebb mint az MPI-é, ezért már kisebb feladatoknál is érdemes lehet használni.
Az előre várt tendenciát kizárólag a hard.sdk esetében látjuk, ez azért van mert itt található egyedül elég sok sudoku, hogy a kommunikációs overhead ne legyen domináns.
