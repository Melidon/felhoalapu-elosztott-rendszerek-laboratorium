#!/bin/bash

#SBATCH -A p_parhpc
#SBATCH --job-name=sudoku
#SBATCH --time=10

#SBATCH -o std.out
#SBATCH -e std.err
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=1

$@ build/sudoku `cat hard.sdk`
