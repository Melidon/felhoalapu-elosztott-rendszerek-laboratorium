#!/bin/bash

mkdir -p build && \
cd build && \
cmake -DCMAKE_BUILD_TYPE=Release -DCMAKE_CXX_COMPILER=/usr/bin/mpic++ -S .. && \
cd .. && \
cmake --build build && \
sbatch --nodes=4 run.sh mpirun
