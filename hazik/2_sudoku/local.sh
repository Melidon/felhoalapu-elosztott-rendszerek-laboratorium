#!/bin/bash

cmake -DCMAKE_BUILD_TYPE=Debug -DCMAKE_EXPORT_COMPILE_COMMANDS=TRUE -B build && \
cmake --build build && \
./run.sh > std.out 2> std.err
