#!/bin/bash

cmake -DCMAKE_BUILD_TYPE=Release -B build && \
cmake --build build && \
sbatch --nodes=4 run.sh srun
