#include "filter.h"

#include <complex>
#include <stdexcept>

using complex = std::complex<double>;

static auto operator*(const Image<complex> &lhs, const Image<complex> &rhs) -> Image<complex> {
    if (lhs.get_width() != rhs.get_width() || lhs.get_height() != rhs.get_height()) {
        throw std::invalid_argument{"Images must have the same dimensions to be multiplied."};
    }
    auto output = Image<complex>{
        std::vector<complex>(lhs.get_width() * lhs.get_height()),
        lhs.get_width(),
        lhs.get_height(),
    };
    for (std::size_t y = 0; y < output.get_height(); ++y) {
        for (std::size_t x = 0; x < output.get_width(); ++x) {
            output.at(x, y) = lhs.at(x, y) * rhs.at(x, y);
        }
    }
    return output;
}

static constexpr std::size_t filter_ratio = 32;

auto filter(const Image<std::complex<double>> &input, FilterType filter_type) -> Image<std::complex<double>> {
    const auto width = input.get_width();
    const auto height = input.get_height();
    const auto filter_width = width / filter_ratio;
    const auto filter_height = height / filter_ratio;
    auto filter = Image<complex>{
        std::vector<complex>(width * height),
        width,
        height,
    };
    for (std::size_t y = 0; y < height; ++y) {
        for (std::size_t x = 0; x < width; ++x) {
            auto condition = (x >= filter_width && x < width - filter_width && y >= filter_height && y < height - filter_height);
            filter.at(x, y) = condition ? complex{1} : complex{0};
            if (filter_type == FilterType::LowPass) {
                filter.at(x, y) = complex{1} - filter.at(x, y);
            }
        }
    }
    return input * filter;
}
