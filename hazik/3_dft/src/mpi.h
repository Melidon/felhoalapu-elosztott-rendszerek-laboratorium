#pragma once

#include <stdexcept>
#include <utility>
#include <vector>

#include <mpi.h>

namespace mpi {

using Rank = int;
using Tag = int;

class Error : public std::runtime_error {
public:
    explicit Error(int errorcode);
};

class Status {
    MPI_Status status;

public:
    explicit Status(MPI_Status status);
    Status(const Status &) = default;
    Status(Status &&) noexcept = default;
    Status &operator=(const Status &) = default;
    Status &operator=(Status &&) noexcept = default;
    ~Status() = default;

    template <typename T>
    [[nodiscard]] auto count() const -> int;
};

class Message {
    MPI_Message message;

public:
    explicit Message(MPI_Message message);
    Message(const Message &) = default;
    Message(Message &&) noexcept = default;
    Message &operator=(const Message &) = default;
    Message &operator=(Message &&) noexcept = default;
    ~Message() = default;

    template <typename T>
    [[nodiscard]] auto matched_receive_into(std::vector<T> &buf) -> Status;
};

class SimpleCommunicator;

class Process {
    const SimpleCommunicator &comm;
    const Rank rank;

    friend class SimpleCommunicator;
    explicit Process(const SimpleCommunicator &comm, Rank rank);

public:
    Process(const Process &) = default;
    Process(Process &&) noexcept = default;
    Process &operator=(const Process &) = delete;
    Process &operator=(Process &&) noexcept = delete;
    ~Process() = default;

    template <typename T>
    auto send(const std::vector<T> &buf) const -> void;

    template <typename T>
    auto send_with_tag(const std::vector<T> &buf, Tag tag) const -> void;

    template <typename T>
    [[nodiscard]] auto receive_vec() const -> std::pair<std::vector<T>, Status>;

    template <typename T>
    [[nodiscard]] auto receive_vec_with_tag(Tag tag) const -> std::pair<std::vector<T>, Status>;

private:
    [[nodiscard]] auto matched_probe_with_tag(Tag tag) const -> std::pair<Message, Status>;
};

class SimpleCommunicator {
    const MPI_Comm comm;

    friend class Universe;
    explicit SimpleCommunicator(MPI_Comm comm);

public:
    SimpleCommunicator(const SimpleCommunicator &) = delete;
    SimpleCommunicator(SimpleCommunicator &&) noexcept = default;
    SimpleCommunicator &operator=(const SimpleCommunicator &) = delete;
    SimpleCommunicator &operator=(SimpleCommunicator &&) noexcept = delete;
    ~SimpleCommunicator() = default;

    [[nodiscard]] auto as_raw() const -> MPI_Comm;

    [[nodiscard]] auto rank() const -> Rank;
    [[nodiscard]] auto size() const -> Rank;

    [[nodiscard]] auto process_at_rank(Rank rank) const -> Process;
};

class Universe {
    bool moved = false;

    friend auto initialize(int *argc, char ***argv) -> Universe;
    explicit Universe() = default;

public:
    Universe(const Universe &) = delete;
    Universe(Universe &&rhs) noexcept;
    Universe &operator=(const Universe &) = delete;
    Universe &operator=(Universe &&rhs) noexcept;
    ~Universe();

    [[nodiscard]] auto world() const -> SimpleCommunicator;
};

[[nodiscard]] auto initialize(int *argc, char ***argv) -> Universe;

} // namespace mpi
