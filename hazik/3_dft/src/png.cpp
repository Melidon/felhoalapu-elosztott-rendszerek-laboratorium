#include "png.h"

#include "lodepng.h"

auto loadPNG(const char *filename) -> Image<> {
    auto data = std::vector<std::uint8_t>{};
    auto width = 0u;
    auto height = 0u;
    if (const auto error = lodepng::decode(data, width, height, filename, LCT_GREY); error != 0) {
        auto error_text = lodepng_error_text(error);
        throw LodepngError(error_text);
    }
    return Image<>{
        std::move(data),
        static_cast<std::size_t>(width),
        static_cast<std::size_t>(height),
    };
}

auto savePNG(const char *filename, const Image<> &image) -> void {
    if (const auto error = lodepng::encode(filename, image.get_data(), static_cast<unsigned>(image.get_width()), static_cast<unsigned>(image.get_height()), LCT_GREY); error != 0) {
        auto error_text = lodepng_error_text(error);
        throw LodepngError(error_text);
    }
}
