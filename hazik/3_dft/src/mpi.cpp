#include "mpi.h"

#include <complex>

#include <mpi.h>

namespace mpi {

static auto error_string(int errorcode) -> std::string {
    auto string = std::string(MPI_MAX_ERROR_STRING, '\0');
    if (int length; MPI_Error_string(errorcode, string.data(), &length) != 0) {
        return "Unknown error";
    }
    return string;
}

Error::Error(int error_code) : std::runtime_error{error_string(error_code)} {}

Status::Status(MPI_Status status) : status{status} {}

template <typename T>
[[nodiscard]] auto Status::count() const -> int {
    int count;
    if (const auto errorcode = MPI_Get_count(&status, MPI_BYTE, &count); errorcode != 0) {
        throw Error{errorcode};
    }
    return count / static_cast<int>(sizeof(T));
}

Message::Message(MPI_Message message) : message{message} {}

template <typename T>
[[nodiscard]] auto Message::matched_receive_into(std::vector<T> &buf) -> Status {
    MPI_Status status;
    if (const auto errorcode = MPI_Mrecv(buf.data(), static_cast<int>(buf.size() * sizeof(T)), MPI_BYTE, &message, &status); errorcode != 0) {
        throw Error{errorcode};
    }
    return Status{status};
}

Process::Process(const SimpleCommunicator &comm, Rank rank) : comm{comm}, rank{rank} {}

template <typename T>
auto Process::send(const std::vector<T> &buf) const -> void {
    send_with_tag(buf, 0);
}

template <typename T>
auto Process::send_with_tag(const std::vector<T> &buf, Tag tag) const -> void {
    if (const auto errorcode = MPI_Send(buf.data(), static_cast<int>(buf.size() * sizeof(T)), MPI_BYTE, rank, tag, comm.as_raw()); errorcode != 0) {
        throw Error{errorcode};
    }
}

template <typename T>
[[nodiscard]] auto Process::receive_vec() const -> std::pair<std::vector<T>, Status> {
    return receive_vec_with_tag<T>(0);
}

template <typename T>
[[nodiscard]] static auto matched_receive_vec(Message message, Status status) -> std::pair<std::vector<T>, Status> {
    const auto count = status.count<T>();
    auto res = std::vector<T>(static_cast<std::size_t>(count));
    const auto status2 = message.matched_receive_into(res);
    return std::make_pair(res, status2);
}

template <typename T>
[[nodiscard]] auto Process::receive_vec_with_tag(Tag tag) const -> std::pair<std::vector<T>, Status> {
    const auto [message, status] = matched_probe_with_tag(tag);
    return matched_receive_vec<T>(message, status);
}

[[nodiscard]] auto Process::matched_probe_with_tag(Tag tag) const -> std::pair<Message, Status> {
    MPI_Message message;
    MPI_Status status;
    if (const auto errorcode = MPI_Mprobe(rank, tag, comm.as_raw(), &message, &status); errorcode != 0) {
        throw Error{errorcode};
    }
    return std::make_pair(Message{message}, Status{status});
}

SimpleCommunicator::SimpleCommunicator(MPI_Comm comm) : comm{comm} {}

[[nodiscard]] auto SimpleCommunicator::as_raw() const -> MPI_Comm {
    return comm;
}

[[nodiscard]] auto SimpleCommunicator::rank() const -> Rank {
    int rank;
    if (const auto errorcode = MPI_Comm_rank(comm, &rank); errorcode != 0) {
        throw Error{errorcode};
    }
    return rank;
}

[[nodiscard]] auto SimpleCommunicator::size() const -> Rank {
    int size;
    if (const auto errorcode = MPI_Comm_size(comm, &size); errorcode != 0) {
        throw Error{errorcode};
    }
    return size;
}

[[nodiscard]] auto SimpleCommunicator::process_at_rank(Rank rank) const -> Process {
    return Process{*this, rank};
}

Universe::Universe(Universe &&rhs) noexcept {
    rhs.moved = true;
}

Universe &Universe::operator=(Universe &&rhs) noexcept {
    if (this != &rhs) {
        moved = rhs.moved;
        rhs.moved = true;
    }
    return *this;
}

Universe::~Universe() {
    if (!moved) {
        MPI_Finalize();
    }
}

[[nodiscard]] auto Universe::world() const -> SimpleCommunicator {
    return SimpleCommunicator{MPI_COMM_WORLD};
}

[[nodiscard]] auto initialize(int *argc, char ***argv) -> Universe {
    if (const auto errorcode = MPI_Init(argc, argv); errorcode != 0) {
        throw Error{errorcode};
    }
    return Universe{};
}

// Explicit instantiations
template auto Process::send(const std::vector<std::complex<double>> &buf) const -> void;
template auto Process::receive_vec() const -> std::pair<std::vector<std::complex<double>>, Status>;

} // namespace mpi
