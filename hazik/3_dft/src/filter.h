#pragma once

#include <complex>

#include "image.h"

enum class FilterType {
    LowPass,
    HighPass,
};

auto filter(const Image<std::complex<double>> &input, FilterType filter_type) -> Image<std::complex<double>>;
