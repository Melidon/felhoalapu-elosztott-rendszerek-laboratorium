#include "dft.h"

using complex = std::complex<double>;

static auto _DFT(const Image<complex> &input, const mpi::SimpleCommunicator &comm, bool horizontal, bool inverse) -> Image<complex> {
    auto output = Image<complex>{
        std::vector<complex>(input.get_width() * input.get_height()),
        input.get_width(),
        input.get_height(),
    };
    const auto input_lines = horizontal ? input.rows() : input.cols();
    auto output_lines = horizontal ? output.rows() : output.cols();
    const auto size = comm.size();
    const auto rank = comm.rank();
    for (mpi::Rank rank_i = 0; rank_i < size; ++rank_i) {
        const std::size_t begin = input_lines.size() * static_cast<std::size_t>(rank_i) / static_cast<std::size_t>(size);
        const std::size_t end = input_lines.size() * static_cast<std::size_t>(rank_i + 1) / static_cast<std::size_t>(size);
        if (rank == rank_i) {
#pragma omp parallel for schedule(static)
            for (std::size_t line_i = begin; line_i < end; ++line_i) {
                const auto &x = input_lines[line_i];
                auto &X = output_lines[line_i];
                for (std::size_t k = 0; k < x.size(); ++k) {
                    const auto N = x.size();
                    auto sum = complex{0, 0};
                    for (std::size_t n = 0; n <= N - 1; ++n) {
                        sum += x[n] * std::exp(complex{0, (inverse ? 2.0 : -2.0) * M_PI * static_cast<double>(k) / static_cast<double>(N) * static_cast<double>(n)});
                    }
                    if (inverse) {
                        sum /= static_cast<double>(N);
                    }
                    X[k] = sum;
                }
            }
            if (rank_i > 0) {
                comm.process_at_rank(0).send(output.get_data());
            }
        }
        if (rank_i > 0 && rank == 0) {
            const auto buf = comm.process_at_rank(rank_i).receive_vec<complex>().first;
            const auto received = Image<complex>{buf, input.get_width(), input.get_height()};
            const auto received_lines = horizontal ? received.rows() : received.cols();
            for (std::size_t line_i = begin; line_i < end; ++line_i) {
                for (std::size_t k = 0; k < received_lines.size(); ++k) {
                    output_lines[line_i][k] = received_lines[line_i][k];
                }
            }
        }
    }
    for (mpi::Rank rank_i = 1; rank_i < size; ++rank_i) {
        if (rank == 0) {
            comm.process_at_rank(rank_i).send(output.get_data());
        }
        if (rank == rank_i) {
            const auto buf = comm.process_at_rank(0).receive_vec<complex>().first;
            output = Image<complex>{buf, input.get_width(), input.get_height()};
        }
    }
    return output;
}

auto DFT(const Image<complex> &input, const mpi::SimpleCommunicator &comm) -> Image<complex> {
    return _DFT(_DFT(input, comm, true, false), comm, false, false);
}

auto inverse_DFT(const Image<complex> &input, const mpi::SimpleCommunicator &comm) -> Image<complex> {
    return _DFT(_DFT(input, comm, false, true), comm, true, true);
}
