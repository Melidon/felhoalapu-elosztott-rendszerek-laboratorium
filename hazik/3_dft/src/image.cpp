#include "image.h"

#include <complex>

template <class Pixel>
Image<Pixel>::Image(std::vector<Pixel> data, std::size_t width, std::size_t height) : data{std::move(data)}, width{width}, height{height} {
    if (this->data.size() != width * height) {
        throw std::invalid_argument{"Invalid data size"};
    }
}

template <class Pixel>
auto Image<Pixel>::get_data() -> std::vector<Pixel> & {
    return data;
}

template <class Pixel>
auto Image<Pixel>::get_data() const -> const std::vector<Pixel> & {
    return data;
}

template <class Pixel>
auto Image<Pixel>::get_width() const -> std::size_t {
    return width;
}

template <class Pixel>
auto Image<Pixel>::get_height() const -> std::size_t {
    return height;
}

template <class Pixel>
auto Image<Pixel>::at(std::size_t x, std::size_t y) -> Pixel & {
    return data[y * width + x];
}

template <class Pixel>
auto Image<Pixel>::at(std::size_t x, std::size_t y) const -> const Pixel & {
    return data[y * width + x];
}

template <class Pixel>
Image<Pixel>::Line::Line(Image &image, std::size_t nth, bool horizontal) : image{image}, nth{nth}, horizontal{horizontal} {}

template <class Pixel>
auto Image<Pixel>::Line::size() const -> std::size_t {
    return horizontal ? image.width : image.height;
}

template <class Pixel>
auto Image<Pixel>::Line::operator[](std::size_t index) -> Pixel & {
    if (horizontal) {
        return image.data[nth * image.width + index];
    } else {
        return image.data[index * image.width + nth];
    }
}

template <class Pixel>
auto Image<Pixel>::Line::operator[](std::size_t index) const -> const Pixel & {
    if (horizontal) {
        return image.data[nth * image.width + index];
    } else {
        return image.data[index * image.width + nth];
    }
}

template <class Pixel>
Image<Pixel>::ConstLine::ConstLine(const Image &image, std::size_t nth, bool horizontal) : image{image}, nth{nth}, horizontal{horizontal} {}

template <class Pixel>
auto Image<Pixel>::ConstLine::size() const -> std::size_t {
    return horizontal ? image.width : image.height;
}

template <class Pixel>
auto Image<Pixel>::ConstLine::operator[](std::size_t index) const -> const Pixel & {
    if (horizontal) {
        return image.data[nth * image.width + index];
    } else {
        return image.data[index * image.width + nth];
    }
}

template <class Pixel>
auto Image<Pixel>::rows() -> std::vector<Line> {
    auto rows = std::vector<Line>{};
    rows.reserve(height);
    for (std::size_t i = 0; i < height; ++i) {
        rows.emplace_back(*this, i, true);
    }
    return rows;
}

template <class Pixel>
auto Image<Pixel>::rows() const -> std::vector<ConstLine> {
    auto rows = std::vector<ConstLine>{};
    rows.reserve(height);
    for (std::size_t i = 0; i < height; ++i) {
        rows.emplace_back(*this, i, true);
    }
    return rows;
}

template <class Pixel>
auto Image<Pixel>::cols() -> std::vector<Line> {
    auto cols = std::vector<Line>{};
    cols.reserve(width);
    for (std::size_t i = 0; i < width; ++i) {
        cols.emplace_back(*this, i, false);
    }
    return cols;
}

template <class Pixel>
auto Image<Pixel>::cols() const -> std::vector<ConstLine> {
    auto cols = std::vector<ConstLine>{};
    cols.reserve(width);
    for (std::size_t i = 0; i < width; ++i) {
        cols.emplace_back(*this, i, false);
    }
    return cols;
}

// Explicit instantiations
template class Image<std::uint8_t>;
template class Image<std::complex<double>>;
