#include <chrono>
#include <iostream>

#include "convert.h"
#include "dft.h"
#include "filter.h"
#include "mpi.h"
#include "png.h"

int main(int argc, char **argv) {
    const auto universe = mpi::initialize(&argc, &argv);
    const auto world = universe.world();
    const auto rank = world.rank();
    if (argc <= 1) {
        std::cerr << "Usage: " << argv[0] << " <image.png>" << std::endl;
        return EXIT_FAILURE;
    }
    for (int i = 1; i < argc; i++) {
        try {
            using namespace std::chrono;
            const auto image_path = std::string{argv[i]};
            const auto last_deliminator = image_path.find_last_of('/');
            const auto parent_path = image_path.substr(0, last_deliminator + 1);
            const auto filename = image_path.substr(last_deliminator + 1, image_path.size());
            if (rank == 0) {
                std::cout << "Loading " << image_path << "..." << std::endl;
            }
            const auto image = real_to_complex(loadPNG(image_path.c_str()));
            if (rank == 0) {
                std::cout << "Calling DFT on " << image_path << "..." << std::endl;
            }
            const auto start = high_resolution_clock::now();
            const auto transformed = DFT(image, world);
            const auto end = high_resolution_clock::now();
            if (rank == 0) {
                std::cout << "DFT finished in " << duration_cast<milliseconds>(end - start).count() << "ms" << std::endl;
            }

            if (rank == 0) {
                std::cout << "Low pass filtering " << image_path << "..." << std::endl;
            }
            const auto low_pass_filtered_transformed = filter(transformed, FilterType::LowPass);
            if (rank == 0) {
                std::cout << "Low pass filtering finished. " << std::endl;
                std::cout << "Calling inverse DFT on low pass filtered image..." << std::endl;
            }
            const auto low_pass_filtered_image = inverse_DFT(low_pass_filtered_transformed, world);
            if (rank == 0) {
                std::cout << "Inverse DFT finished." << std::endl;
                const auto low_pass_filtered_image_path = parent_path + "low_pass_filtered_" + filename;
                std::cout << "Saving as " << low_pass_filtered_image_path << "..." << std::endl;
                savePNG(low_pass_filtered_image_path.c_str(), complex_to_real(low_pass_filtered_image));
            }

            if (rank == 0) {
                std::cout << "High pass filtering " << image_path << "..." << std::endl;
            }
            const auto high_pass_filtered_transformed = filter(transformed, FilterType::HighPass);
            if (rank == 0) {
                std::cout << "High pass filtering finished. " << std::endl;
                std::cout << "Calling inverse DFT on high pass filtered image..." << std::endl;
            }
            const auto high_pass_filtered_image = inverse_DFT(high_pass_filtered_transformed, world);
            if (rank == 0) {
                std::cout << "Inverse DFT finished." << std::endl;
                const auto high_pass_filtered_image_path = parent_path + "high_pass_filtered_" + filename;
                std::cout << "Saving as " << high_pass_filtered_image_path << "..." << std::endl;
                savePNG(high_pass_filtered_image_path.c_str(), complex_to_real(high_pass_filtered_image));
                std::cout << std::endl;
            }
        } catch (const LodepngError &error) {
            std::cerr << error.what() << std::endl << std::endl;
            continue;
        }
    }
    if (rank == 0) {
        std::cout << "Done!" << std::endl;
    }
    return EXIT_SUCCESS;
}
