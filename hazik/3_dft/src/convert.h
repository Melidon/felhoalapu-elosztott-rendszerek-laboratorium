#pragma once

#include <complex>

#include "image.h"

// Convert bytes to complex
auto real_to_complex(const Image<> &real_image) -> Image<std::complex<double>>;

// Convert complex to bytes
auto complex_to_real(const Image<std::complex<double>> &complex_image) -> Image<>;

// Convert complex to bytes and scale to 0-255
auto complex_to_rescaled_real(const Image<std::complex<double>> &complex_image, bool logarithmic = false) -> Image<>;
