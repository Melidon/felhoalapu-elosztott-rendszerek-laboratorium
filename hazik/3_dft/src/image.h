#pragma once

#include <cstdint>
#include <vector>

template <class Pixel = std::uint8_t>
class Image {
private:
    std::vector<Pixel> data;
    std::size_t width;
    std::size_t height;

public:
    Image(std::vector<Pixel> data, std::size_t width, std::size_t height);
    Image(const Image &) = delete;
    Image(Image &&) noexcept = default;
    Image &operator=(const Image &) = delete;
    Image &operator=(Image &&) noexcept = default;

    auto get_data() -> std::vector<Pixel> &;
    auto get_data() const -> const std::vector<Pixel> &;
    auto get_width() const -> std::size_t;
    auto get_height() const -> std::size_t;
    auto at(std::size_t x, std::size_t y) -> Pixel &;
    auto at(std::size_t x, std::size_t y) const -> const Pixel &;

    class Line {
    private:
        Image &image;
        std::size_t nth;
        bool horizontal;

    public:
        Line(Image &image, std::size_t nth, bool horizontal);
        auto size() const -> std::size_t;
        auto operator[](std::size_t index) -> Pixel &;
        auto operator[](std::size_t index) const -> const Pixel &;
    };

    class ConstLine {
    private:
        const Image &image;
        std::size_t nth;
        bool horizontal;

    public:
        ConstLine(const Image &image, std::size_t nth, bool horizontal);
        auto size() const -> std::size_t;
        auto operator[](std::size_t index) const -> const Pixel &;
    };

    auto rows() -> std::vector<Line>;
    auto rows() const -> std::vector<ConstLine>;
    auto cols() -> std::vector<Line>;
    auto cols() const -> std::vector<ConstLine>;
};
