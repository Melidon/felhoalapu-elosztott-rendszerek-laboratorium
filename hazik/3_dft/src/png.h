#pragma once

#include <stdexcept>

#include "image.h"

// Load the image
auto loadPNG(const char *filename) -> Image<>;

// Save the image
auto savePNG(const char *filename, const Image<> &image) -> void;

class LodepngError : public std::runtime_error {
    using std::runtime_error::runtime_error;
};
