#include "convert.h"

using complex = std::complex<double>;

auto real_to_complex(const Image<> &real_image) -> Image<complex> {
    auto complex_image = Image<complex>{
        std::vector<complex>(real_image.get_width() * real_image.get_height()),
        real_image.get_width(),
        real_image.get_height(),
    };
    for (std::size_t i = 0; i < real_image.get_width() * real_image.get_height(); ++i) {
        complex_image.get_data()[i] = complex{static_cast<double>(real_image.get_data()[i])};
    }
    return complex_image;
}

auto complex_to_real(const Image<complex> &complex_image) -> Image<> {
    auto real_image = Image<>{
       std::vector<std::uint8_t>(complex_image.get_width() * complex_image.get_height()),
        complex_image.get_width(),
        complex_image.get_height(),
    };
    for (std::size_t i = 0; i < complex_image.get_width() * complex_image.get_height(); ++i) {
        real_image.get_data()[i] = static_cast<std::uint8_t>(std::abs(complex_image.get_data()[i]));
    }
    return real_image;
}

auto complex_to_rescaled_real(const Image<complex> &complex_image, bool logarithmic) -> Image<> {
    auto real_image = Image<>{
        std::vector<std::uint8_t>(complex_image.get_width() * complex_image.get_height()),
        complex_image.get_width(),
        complex_image.get_height(),
    };
    auto max = 0.0;
    for (std::size_t i = 0; i < complex_image.get_width() * complex_image.get_height(); ++i) {
        auto length = std::abs(complex_image.get_data()[i]);
        if (max < length) {
            max = length;
        }
    }
    if (logarithmic) {
        max = log(max + 1);
    }
    auto scale = 1.0;
    if (max != 0) {
        scale = 255 / max;
    }
    for (std::size_t i = 0; i < complex_image.get_width() * complex_image.get_height(); ++i) {
        auto length = std::abs(complex_image.get_data()[i]);
        if (logarithmic) {
            length = log(length + 1);
        }
        length = scale * length;
        real_image.get_data()[i] = static_cast<std::uint8_t>(length);
    }
    return real_image;
}
