#pragma once

#include <complex>

#include "image.h"
#include "mpi.h"

// 2D DFT
auto DFT(const Image<std::complex<double>> &input, const mpi::SimpleCommunicator &comm) -> Image<std::complex<double>>;

// 2D inverse DFT
auto inverse_DFT(const Image<std::complex<double>> &input, const mpi::SimpleCommunicator &comm) -> Image<std::complex<double>>;
