#!/bin/bash

#SBATCH -A p_parhpc
#SBATCH --job-name=dft
#SBATCH --time=10

#SBATCH -o std.out
#SBATCH -e std.err
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=1

OMP_NUM_THREADS=$SLURM_CPUS_PER_TASK $1 build/dft images/img.png images/lena.png images/lena2.png images/lena3.png images/stripes8.png images/stripes80.png images/stripes80R.png
