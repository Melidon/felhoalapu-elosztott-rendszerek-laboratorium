# DFT (Problem #6)
## Goal
To learn about the importance of DFT in image processing. To implement an efficient parallel DFT algorithm.

## Tools, conditions
The solution must work on the **para** cluster with **MPI** parallelization.

## Help
Contents of the DFT.tgz file:
- **DFT.cpp**: Source of DFT algorithm and some helper functions.
- **lodepng.cpp**: Functions for writing/reading PNG files.
- **\*.png**: png image files for testing.
- **Makefile** Help for compiling.

## Tasks:
- Study the theoretical basics of DFT.
- Understand and run the dft program on the supplied image files.
- Create an efficient parallel DFT algorithm that can be applied to large images.
- Create a filter and apply it in the frequency domain.
- Measure the speedup and prepare the documentation.

## To be submitted
Source files of the solution and a documentation/presentation describing the solution and measurement method, including the measurement results and their interpretation.


