#  Diszkrét Fourier transzformáció (Feladat #3) -> 3_dft

## Bálint Gergő (O78UXU), Szommer Zsombor (MM5NOT)

## Cél
Megismerkedni a DFT jelentőségével a képfeldolgozásban. Egy hatékony párhuzamos DFT algoritmus implementálása.

## Feladatok: 
- Tanulmányozza a DFT elméleti alalpjait!
- Értse meg és futtassa a mellékelt képfájlokra a **dft** programot!
- Készítsen egy hatékony párhuzamos DFT algoritmust, ami nagyméretű képekre is alkalmazható.
- Készítsen filtert és alkalmazza azt a frekvencia domainben!
- Végezzen méréseket, és készítse el a dokumentációt!

## Beadott file-ok:

- **README.pdf**
- **libs/lodepng**
  - **include/**
    - **lodepng.h**
  - **src/**
    - **lodepng.cpp**
  - **CMakeLists.txt**
  - **LICENSE**
- **src/**
  - **convert.cpp**
  - **convert.h**
  - **dft.cpp**
  - **dft.h**
  - **filter.cpp**
  - **filter.h**
  - **image.cpp**
  - **image.h**
  - **main.cpp**
  - **mpi.cpp**
  - **mpi.h**
  - **png.cpp**
  - **png.h**
- **CMakeLists.txt**
- **komondor.sh**
- **local.sh**
- **para.sh**
- **run.sh**

## Feladat kivitelezése

A kapott soros végrehajtású programot először átstruktúráltuk.
Függvényekbe szerveztük ki az arra érdemes részeket, és modern C++ elemekre cseréltünk mindent amit lehetett.
Ennek a megoldásnak saját **laptopon**, illetve a **para** és a **komondor** szervereken is futnia kell, ezt három `*.sh` fájl megadásával választottuk külön (local.sh, para.sh, komondor.sh).

A feladat kérte, hogy készítsünk filtert, ezért készítetünk egy aluláteresztő, és egy felüláteresztő szűrőt.

### low pass filtered lena:

Látható, hogy a szűrő összemosta az éleket.

![LowPass](images/low_pass_filtered_lena.png)

### high pass filtered lena:

Látható, hogy a szűrő csak az éleket hagyta meg.

![HighPass](images/high_pass_filtered_lena.png)

Ezután megvalósítottuk a párhuzamosítást is.
A megoldásunk **OpenMP**-t tartalmaz a szálkezelés egyszerűsítésére a gyorsításhoz, illetve **MPI**-t a példánytöbbszörösítésre.
A megoldást **Slurm** segítségével szétosztva tudjuk futtatni a szervergép node-okon.

Feladat volt az elért gyorsulás kimérése, és dokumentálása is.
Az időmérést `std::chrono::high_resolution_clock` segítségével végeztük a head node-on.
Különböző paraméterek változtatásával kimértük a futási időket saját laptopon, a para szerveren és a komondoron, mérési eredményeink a következő táblázatban láthatók:

## Eredmények

A program futása során háromszor történik fourier-transzformáció, mivel először transzformálunk, alkalmazzuk a két filterünket (szűrés) majd ezeket visszatranszformáljuk, így jön ki a három transzformáció.
A táblázatban feltüntetett értékek egy fourier-transzformáció idejét mutatja az adott paraméterek mellett.

### Laptop

| Fájlnév        | CPU=1 * N=1 | CPU=1 * N=4 | CPU=4 * N=1 | CPU=4 * N=4 |
| -------------- | ----------: | ----------: | ----------: | ----------: |
| img.png        |      400 ms |           - |      132 ms |           - |
| lena.png       |    25320 ms |           - |     6517 ms |           - |
| lena2.png      |     9345 ms |           - |     2485 ms |           - |
| lena3.png      |  1408728 ms |           - |   359289 ms |           - |
| stripes8.png   |    27045 ms |           - |     6582 ms |           - |
| stripes80.png  |    24691 ms |           - |     6488 ms |           - |
| stripes80R.png |    26342 ms |           - |     6465 ms |           - |

### Para

| Fájlnév        | CPU=1 * N=1 | CPU=1 * N=4 | CPU=4 * N=1 | CPU=4 * N=4 |
| -------------- | ----------: | ----------: | ----------: | ----------: |
| img.png        |      519 ms |      177 ms |      130 ms |       75 ms |
| lena.png       |    27911 ms |     7488 ms |     7131 ms |     2337 ms |
| lena2.png      |    10284 ms |     2796 ms |     2627 ms |      912 ms |
| lena3.png      |  1573819 ms |   397985 ms |   401491 ms |   109987 ms |
| stripes8.png   |    27951 ms |     7447 ms |     7087 ms |     2251 ms |
| stripes80.png  |    28014 ms |     7457 ms |     7118 ms |     2288 ms |
| stripes80R.png |    28019 ms |     7463 ms |     7099 ms |     2259 ms |

### Komondor

| Fájlnév        | CPU=1 * N=1 | CPU=1 * N=4 | CPU=4 * N=1 | CPU=4 * N=4 |
| -------------- | ----------: | ----------: | ----------: | ----------: |
| img.png        |      186 ms |       34 ms |       36 ms |       11 ms |
| lena.png       |    10592 ms |     1977 ms |     2144 ms |      531 ms |
| lena2.png      |     3952 ms |      750 ms |      812 ms |      207 ms |
| lena3.png      |   595185 ms |   115980 ms |   112971 ms |    28056 ms |
| stripes8.png   |    10493 ms |     2075 ms |     1949 ms |      515 ms |
| stripes80.png  |    11780 ms |     2177 ms |     1954 ms |      510 ms |
| stripes80R.png |    11787 ms |     2240 ms |     1952 ms |      820 ms |

## Összegzés

A következő tanulságot tudjuk levonni.
Az elvárt eredményeket kaptuk meg a mérés során nagyjából négyszeres, illetve tizenhatszoros gyorsulást értünk el a kipróbált konfigurációkkal.
A karakterisztika lineáris.
A feladat számításigénye miatt az előző (SUDOKU) feladattal ellentétben nem jelent meg releváns kommunikációs overhead az OpenMP miatt, elhanyagolható.

Ebben a feladatban a futási idő a kép méretének negyedik hatványával arányos, mivel mindegyik pixel függ mindegyiktől, így egy kimenet kiszámolása két dimenzióban kétszer akkora képen négyszer annyi idő és négy kimenetet számolunk, így kapjuk a 16-szoros növekedést ha kétszerezzük a képméretet.
