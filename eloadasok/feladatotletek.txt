Problem groups:
  * N-Body simulation
  * Parallel sorting
  * Parallel heuristic search algorithms
  * Algorithms for an optimization problem such as the travelling
    salesman problem
  * Minimax search
  * An evolutionary algorithm
  * Graph algorithms
  * Picture processing


Distribution of Matrix elements:
    Given is a matrix of NxN integers, initialized to random values
    between 1 and 10. The problem is to calculate the ultimate collapse
    of values in rows, columns and all the elements. Write a parallel
    program solve the problem.
    The collapse of a set of integers is defined to be the sum of the
    collapse values of the integers in the set. Similarly, the collapse
    of a single integer is defined to be the integer that is the sum of
    its digits. For example, the collapse of the integer 134957 is 29.
    This can clearly be earned out recursively, until a single digit
    results: the collapse of 29 is 11, and its collapse is the single
    digit 2. The ultimate collapse of a set of integers is just their
    collapse followed by the result being collapsed recursively until
    only a single digit 0, 1. . . 9) remains. For row i, the output
    should be (ROW, i, ci), if the collapsed value for the row i is ci.
    For column j, the output should be (COL, j, cj ), if the collapsed
    value for the column j is cj . The ultimate collapse value of the
    matrix should be printed as (MATRIX, cm), if cm is the ultimate
    collapse value of the matrix. Read the size of the matrix and the
    number of threads/processes as command line arguments. List the
    different patterns identified during finding concurrency "and
    algorithm design" phase. 


Gaussian Elimination
    Implement and test a parallel program for block Gaussian elimination
    (don't have to explicitly compute L and U factors, but do have to
    solve the linear equation y = Ax for a given y) or block LU (must
    compute the L and U factors (they may overwrite the A matrix, but do
    not have to do the back and forward substitution to solve y = Ax).

    Compare your sulution to any BLAS implementation.


Wandering Salesman
    The wandering salesman problem is a variant of a well known NP-hard
    problem: given the distances between each of n cities, find the
    shortest path that starts at one city and visits all other cites
    exactly once. (The path does not need to return to the start city.)
    The input to the problem is given in the form of a matrix. An
    element of the matrix, d[i][j] gives the distance between city i and
    city j. Specfically, the input to your program is a file organized
    as follows:
    N
    d[2][1]
    d[3][1] d[3][2]
    d[4][1] d[4][2] d[4][3]
    .
    .
    .
    d[N][1] d[N][2] d[n][3] ... d[N][N-1]
    where N is the number of cities, and d[i][j] is an integer giving
    the distance between cities i and j. The output from the program
    should be an ordered list of cities (numbers between 1 and N)
    describing the shortest path. Note that the three-city (n=3)
    orderings (1 -> 2 -> 3 and 3 -> 2 -> 1) correspond to the same
    three-city path, it is just traversed in a different direction. Your
    programs can emit either ordering as a solution. Use the Branch-and-
    Bound Solution to this combinatorial optimization problem:

    First, consider a simple brute-force solution to the the WSP that
    evaluates the cost of all paths. A simple brute-force solution to
    the WSP is to construct trees rooted by each of the n possible
    starting cities, and then, for each tree, compute the cost of all
    leaf paths via traversal from the root to each leaf. The path with
    the smallest distance is the answer to the WSP problem. (Note that
    this brute-force solution tries each unique path twice -- in
    "forward" and "backward" order.) A more work-efficient way to
    traverse each tree is to do it recursively in depth-first order,
    incrementally keeping track of the current path length from the
    root. This depth-first traversal order avoids the need to recompute
    path lengths for common prefixes.

    The Branch-and-Bound approach follows the depth-first traversal
    order, but with some added intelligence. It uses more knowledge of
    the problem to prune subtrees so that less overall evaluation is
    necessary. The basic approach works as follows:
    1. Evaluate one route of the tree in its entirety, and determine the
    distance of that path. Call this distance the current bound of the
    problem. You can think of 'bound' as the length of the best path
    found so far.
    2. Next start evaluating partially the second path and if the
    distance is already greater than the bound there is no need to
    complete the traversal of the subtree rooted by the current path
    because all of those possible routes must have a total distance
    greater than the bound. In this way the subtree is pruned and
    therefore does not require evalution.
    3. Whenever any route is discovered that has a shorter total
    distance than the current bound, then the bound is updated to this
    new value. As the program continues, the bound tightens, the more
    aggresive pruning is possible.
    In summary, a branch-and-bound approach always remembers the best
    path it has found so far, and uses that to prevent useless search
    down parts of the tree that can not possibly produce shorter routes.
    For larger trees, this could result in the removal of many possible
    evaluations.


Sharks and Fish
    An ocean modell in a discrete event modell represented by a
    rectangular (*N*x*M*) matrix: each element of the matrix corresponds
    to a location in the ocean, which can be empty, contain exactly one
    fish, or contain exactly one shark. The time is subdivided in
    standard units, known as steps.
    Rules:
      * Sharks eat and move. At each step, a shark will look around in
        all the adjacent (nearest neighbor ) cells, and eat a fish if it
        finds one there; then the shark will move to the space
        previously occupied by the fish. If the shark cannot find any
        fish, it will move into one of the empty adjacent cells, if
        there any. [In a rectangular matrix, the nearest-neighbor
        elements are those whose first or second index (but not both)
        differ by exactly one.]
      * Sharks breed and die. If a shark survives for *Sb* or more
        steps, it will breed: if an empty adjacent cell is available, a
        new shark will appear there. The parent will breed again after
        *Sb* steps. However, if a shark has not eaten after *Sd* steps,
        it will die (disappear from the grid).
      * Fish move. At each step, each fish will move at random into one
        of the free adjacent cells, if there are any.
      * Fish breed. If a fish survives for *Fb* steps or more, it will
        breed: if an empty adjacent cell is available, a new fish will
        appear there. The parent will breed again after Fb steps.

    Initial population. At the beginning of the simulation, the grid
    should be filled up randomly with *NS* sharks and *NF* fish. Their
    breeding and starving ages can all be set to zero. Random movement.
    Whenever a shark or fish can choose to operate (move, eat, spawn) in
    more than one direction, it should do so randomly.

