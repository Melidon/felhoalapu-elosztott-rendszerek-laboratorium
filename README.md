# Felhő alapú elosztott rendszerek laboratórium

## Bálint Gergő [O78UXU] és Szommer Zsombor [MM5NOT]

Itt a repo gyökerében a negyedik és ötödik házi feladathoz tartozó CI/CD pipeline leíró file található, a házi forrásfile-jai és dokumentációja a repo-n belül található:

[Fotóalbum alkalmazás](https://gitlab.com/Melidon/felhoalapu-elosztott-rendszerek-laboratorium/-/tree/master/hazik/4_fenykepalbum?ref_type=heads)